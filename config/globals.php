<?php

return [
    'NAMESPACE_SEPARATOR' => '\\',
    //    for change user mode to operator section
    //    if login with developer
    'develop_mode' => [
        'status' => false, //get from REQUEST
        'username' => '',  //client@test.com and get from REQUEST
        'developer_identification' => '', //get from DB ip or userAgent or ...
    ],

    'develop_identifications' => [ //set from top
        'ips' => [],
        'user_agents' => [],
    ],

    'app_name' => 'cabin',

    //    for cache section
    'cache' => [
        'status' => false,
        'cache_time' => '1', //per second
    ],

    'refresh_token_expiration' => env('REFRESH_SANCTUM_EXPIRATION', null),
    'reset_pwd_time_exp' => env('RESET_PASSWORD_EXP', 10), // per minutes
    'two_factor_auth_time_exp' => env('TWO_FACTOR_EXP', 11), // per minutes
    'reset_code_time_exp' => env('RESET_PINCODE_EXP', 10), // per minutes
    'expire_trial' => env('Expire_trial', 14), //per days

    //    for log section
    'logs' => [
        'count' => 100, //row
        'table' => 'logs',
    ],

    //    login attempt limiter section
    'login_limiter' => [ // save from caching file(redis)
        'max' => env('LOGIN_LIMITER_MAX', 3), // per times
        'decay' => env('LOGIN_LIMITER_DECAY', 60), // per seconds
    ],

    'api_version' => env('API_VERSION', 'v1'),

    'base_language' => 'en',

    'customer' => [
        'iranian' => 'IR', // iran country abbreviation
        'citizenship' => 'IR', // checking for customer abbreviation country from SetCountryMiddleware
    ],

    'upload_size' => env('UPLOAD_SIZE', 2048), //kb,

    'front' => [
        'base_path' => env('FRONT_ROUTE', 'http://localhost:8080/#/en'),
        'not_found' => env('FRONT_ROUTE', 'http://localhost:8080/#').'/en/error-404',
    ],

    'backend' => [
        'user_api_path' => env('API_USER_ROUTE', 'http://localhost:8080/en/api/v1/user'),
    ],

    'file_mime_type' => env('FILE_MIME_TYPE', ['jpg', 'pdf']),
    'two_factor_exp' => env('TWO_FACTOR_EXP', '10'),
    'default_base_url' => env('DEFAULT_BASE_URL', 'cabin-backend-local.test'),

];
