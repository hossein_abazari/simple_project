<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_generals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained()->on('users')->onDelete('cascade');

            $table->string('file', 70);
            $table->string('id_number');
            $table->date('id_expiration_date');
            $table->foreignId('identity_type_id')->nullable()->constrained()->on('identity_types')->onDelete('cascade');

            $table->boolean('is_verify')->default(0);
            $table->enum('status', ['pending', 'rejected', 'success'])->default('pending');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_generals');
    }
}
