<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('module_id')->nullable()->constrained()->on('modules');
            $table->string('key')->nullable();
            $table->longText('value')->nullable();
            $table->enum('type', ['public', 'private'])->default('private');
            $table->enum('role', ['user', 'admin'])->default('admin');
            $table->timestamps();

            $table->unique('key', 'key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
