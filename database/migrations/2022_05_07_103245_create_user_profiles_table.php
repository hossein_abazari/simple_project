<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->on('users')->onDelete('cascade');
            $table->foreignId('experience_id')->constrained()->on('profile_experiences');
            $table->foreignId('gender_id')->constrained()->on('profile_genders');
            $table->foreignId('hear_id')->constrained()->on('profile_hears');
            $table->foreignId('trade_style_id')->nullable()->constrained()->on('profile_trade_styles');
            $table->foreignId('trade_asset_id')->nullable()->constrained()->on('profile_trade_assets');
            $table->text('hear_desc')->nullable();
            $table->date('birth_date');
            $table->string('profession')->nullable();
            $table->integer('pin_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
