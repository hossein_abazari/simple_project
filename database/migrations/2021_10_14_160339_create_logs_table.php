<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->increments('id');
            $table->text('message')->nullable();
            $table->json('context')->nullable();
            $table->json('extra')->nullable();
            $table->integer('level');
            $table->enum('level_name', ['EMERGENCY', 'ALERT', 'CRITICAL', 'ERROR', 'WARNING', 'NOTICE', 'INFO', 'DEBUG']);
            $table->string('channel');
            $table->ipAddress('ip');
            $table->string('user_agent', 200)->nullable();
            $table->dateTime('record_datetime');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
