<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('abbreviation');
            $table->integer('precode');
            $table->integer('confirmed');
        });

        Schema::create('user_locations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();

            $table->unsignedBigInteger('residency_id');
            $table->foreign('residency_id')->references('id')->on('countries');

            $table->unsignedBigInteger('citizenship_id');
            $table->foreign('citizenship_id')->references('id')->on('countries');

            $table->string('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_locations');
        Schema::dropIfExists('countries');
    }
}
