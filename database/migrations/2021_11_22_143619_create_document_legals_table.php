<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentLegalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_legals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->on('users')->onDelete('cascade');
            $table->foreignId('company_type_id')->constrained()->on('document_company_types')->onDelete('cascade');

            $table->string('company_name');
            $table->integer('company_national_id');
            $table->string('registration_city');
            $table->string('legal_representative_name');
            $table->date('registration_date');
//            $table->string('file', 100)->nullable();

            $table->boolean('is_verify')->default(0);
            $table->enum('status', ['pending', 'rejected', 'success'])->default('pending');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_legals');
    }
}
