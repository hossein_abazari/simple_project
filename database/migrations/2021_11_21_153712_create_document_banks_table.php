<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_banks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->on('users')->onDelete('cascade');

            $table->bigInteger('numbers');
            $table->string('iban');
//            $table->string('expiration_date');
            $table->date('expiration_date');
            $table->string('file', 100)->nullable();

            $table->boolean('is_verify')->default(0);
            $table->enum('status', ['pending', 'rejected', 'success'])->default('pending');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_banks');
    }
}
