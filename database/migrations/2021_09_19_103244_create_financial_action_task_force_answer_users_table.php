<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialActionTaskForceAnswerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_action_task_force_answer_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();

            $table->unsignedBigInteger('question_id');
            $table->foreign('question_id')->references('id')->on('financial_action_task_force_questions');

            $table->tinyInteger('value')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_action_task_force_answer_users');
    }
}
