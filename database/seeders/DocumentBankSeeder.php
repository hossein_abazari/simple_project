<?php

namespace Database\Seeders;

use App\Models\DocumentBank;
use App\Models\DocumentGeneral;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentBank::factory()->count(4)->create();
    }
}
