<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            ['key' => 'app_name', 'value' => 'laravel', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'develop_mode_ips', 'value' => [
                '127.0.0.13',
                '192.168.0.2',
            ], 'type' => 'private', 'role' => 'admin'],
            ['key' => 'develop_mode_user_agents', 'value' => [
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0',
                'PostmanRuntime/7.28.4',
            ], 'type' => 'private', 'role' => 'admin'],
            ['key' => 'developer_credential_switch', 'value' => 'ips', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'language_basic', 'value' => 'fa', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'timezone', 'value' => 'Asia/Dubai', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'caching', 'value' => 'none', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'cache_time', 'value' => '2', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'login_limiter_max', 'value' => '3', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'login_limiter_decay', 'value' => '60', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'logs_count', 'value' => '100', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'reset_pwd_time_exp', 'value' => '5', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'expire_trial', 'value' => '15', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'upload_size', 'value' => '2048', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'file_mime_type', 'value' => [
                'jpg',
                'pdf',
            ], 'type' => 'private', 'role' => 'admin'],
            ['key' => 'two_factor_exp', 'value' => '3', 'type' => 'private', 'role' => 'admin'],
            ['key' => 'default_base_url', 'value' => 'cabin-backend-local.test', 'type' => 'public', 'role' => 'user'],
        ];

        foreach ($arrays as $array) {
            Setting::create(['key' => $array['key'], 'value' => $array['value'], 'type' => $array['type'], 'role' => $array['role']]);
        }
    }
}
