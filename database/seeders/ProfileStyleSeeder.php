<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = ['Scalping', 'Day Trading', 'Swinging', 'A combination of the above'];
        foreach ($arrays as $array) {
            DB::table('profile_trade_styles')->insert(['value' =>$array]);
        }
    }
}
