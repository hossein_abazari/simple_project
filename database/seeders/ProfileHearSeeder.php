<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileHearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = ['Others', 'YouTube', 'Website/Search Engine', 'Twitter', 'TV/Cable News', 'Newspaper Story', 'Magazine Article', 'Family or Friend', 'Facebook', 'Email/Newsletter', 'Advertisement'];

        foreach ($arrays as $array) {
            DB::table('profile_hears')->insert(['value' => $array]);
        }
    }
}
