<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use Illuminate\Database\Seeder;

class LoginQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loginQuestion = LoginQuestion::factory()
            ->count(5)
            ->create();
    }
}
