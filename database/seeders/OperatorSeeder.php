<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orderDesk = Admin::create(
            [
                'name' => 'majid kashefy',
                'email' => 'orderDesk@test.com',
                'email_verified_at' => now(),
                'is_super' => false,
                'is_active' => false,
                'password' => '$2a$10$xfeEXYo1QmjJxgGVobBUCuPHfImRybPFDque0peJ7Q3pBMqewfHUS', // Pass!@#123
                'remember_token' => Str::random(10),
            ]
        );

        $role = Role::create(['name' => 'OrderDesk', 'guard_name'=>'admin']);
        $permissions = Permission::pluck('id', 'id')->skip(4)->take(8);

        $role->syncPermissions($permissions);
        $orderDesk->assignRole([$role->id]);
    }
}
