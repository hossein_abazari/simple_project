<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = ['None', '1-3 Year(s)', '3-5 Year(s)', '5-10 Year(s)', '10 Or More Year(s)'];
        foreach ($arrays as $array) {
            DB::table('profile_experiences')->insert(['value' =>$array]);
        }
    }
}
