<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileGenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = ['Male', 'Female'];
        foreach ($arrays as $array) {
            DB::table('profile_genders')->insert(['value' =>$array]);
        }
    }
}
