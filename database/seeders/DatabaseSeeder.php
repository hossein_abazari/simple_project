<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Kashefy\Modules\ModuleManagement\src\Services\PackageManagerProvider;

class DatabaseSeeder extends Seeder
{
    protected $dataArrays = [
        SettingSeeder::class,
        ProfileAssetSeeder::class,
        ProfileStyleSeeder::class,
        ProfileHearSeeder::class,
        ProfileGenderSeeder::class,
        ProfileExperienceSeeder::class,
        PermissionTableSeeder::class,
        SuperAdminSeeder::class,
        OperatorSeeder::class,
        UserSeeder::class,
        LoginQuestionSeeder::class,
        CountrySeeder::class,
        FATFSeeder::class,
        IdentityTypesSeeder::class,
        DocumentCompanyTypesSeeder::class,
        DocumentGeneralSeeder::class,
        DocumentBankSeeder::class,

    ];

    public function pushModulesSeeder(): array
    {
        $seeders = PackageManagerProvider::getActivatedModulesSeeder();
        foreach ($seeders as $seeder) {
            array_push($this->dataArrays, $seeder);
        }

        return $this->dataArrays;
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $seeders = $this->pushModulesSeeder();
        $this->call($seeders);
    }
}
