<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FATFSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = ['Do you hold nationality of the United States of America?', 'Are you a Resident of the United States of America?', 'Do you hold the United States of America Green Card?', 'Are you a Tax Payer in the United States of America?', 'Were you born in the United States of America?'];
        foreach ($questions as $question) {
            DB::table('financial_action_task_force_questions')->insert(['key' =>$question]);
        }
    }
}
