<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileAssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = ['Commodities', 'Currencies', 'Shares', 'Indices'];
        foreach ($arrays as $array) {
            DB::table('profile_trade_assets')->insert(['value' =>$array]);
        }
    }
}
