<?php

namespace Database\Seeders;

use App\Models\DocumentGeneral;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentGeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentGeneral::factory()->count(3)->create();
    }
}
