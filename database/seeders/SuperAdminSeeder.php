<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $super_admin= Admin::factory()
//            ->count(1)
//            ->create();
        $super_admin = Admin::create(
            [
                'name' => 'majid kashefy',
                'email' => 'superAdmin@test.com',
                'email_verified_at' => now(),
                'is_super' => true,
                'is_active' => true,
                'password' => '$2a$10$xfeEXYo1QmjJxgGVobBUCuPHfImRybPFDque0peJ7Q3pBMqewfHUS', // Pass!@#123
                'remember_token' => Str::random(10),
            ]
        );

        $role = Role::create(['name' => 'SuperAdminw', 'guard_name'=>'admin']);
        $permissions = Permission::pluck('id', 'id')->skip(0)->take(4);

        $role->syncPermissions($permissions);
        $super_admin->assignRole($role->id);
    }
}
