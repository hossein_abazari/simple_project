<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class IdentityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            "Driver's license",
            'Nationality Identification Card',
            'Provisional Card',
            'Passport',
        ];
        foreach ($questions as $question) {
            DB::table('identity_types')->insert(['name' =>$question]);
        }
    }
}
