<?php

namespace Database\Seeders;

use App\Models\LoginQuestion;
use App\Models\Setting;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(database_path().'/seeders/dumps/countries.sql');

        \Illuminate\Support\Facades\DB::statement($sql);
    }
}
