<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Majid',
            'last_name' => 'Kashefy',
            'phone_country_code' => 98,
            'phone' => rand(1000000000, 9999999999),
            'email' => 'kashefymajid1992@gmail.com',
            'password' => '$2a$12$ELvh5OSsYpFqDi8237HnJ.1OgDL1sBELUKURtWZuTSOCM/ZVPZJ/u', // Pass123!@#
            'language' => 'fa',
            'is_active' => 1,
        ]);
        User::factory()->count(10)->create();
    }
}
