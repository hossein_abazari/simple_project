<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentCompanyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            'سایر موارد',
            'سهامی عام',
            'سهامی خاص',
            'مسئولیت محدود',
            'تضامنی',
            'مختلط غیرسهامی',
            'مختلط سهامی',
            'نسبی',
            'تعاونی',
        ];
        foreach ($questions as $question) {
            DB::table('document_company_types')->insert(['name' => $question]);
        }
    }
}
