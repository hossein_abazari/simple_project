<?php

use App\Http\Controllers\Api\v1\User\AuthController as ClientAuthController;
use App\Http\Controllers\Api\v1\User\PinCodeController as ClientPinCodeController;
use App\Http\Controllers\Api\v1\User\ResetPasswordController as ClientResetPasswordController;
use App\Http\Controllers\Api\v1\User\SettingController as ClientSettingController;
use App\Http\Controllers\Api\v1\User\UserController as ClientController;
use App\Http\Controllers\Api\v1\User\UserDocumentController as ClientDocumentController;
use App\Http\Controllers\Api\v1\User\UserProfileController as ClientProfileController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['api', 'client'], 'prefix' => 'user/', 'as' => 'client.'], function () {
//    Route::get('test', function () {
//        dd(Lang::get('admin'));
//    })->name('testttttttttttttttttttttttttttttttttt');


    //    auth routes
    Route::group(['prefix' => 'auth/'], function () {

        // for activation user
        Route::post('active-user', [ClientAuthController::class, 'sendActivationUser'])->name('activate.user');

        // for activation mobile
        Route::post('activation-mobile', [ClientAuthController::class, 'sendActivationMobile'])->name('activate.mobile');
        Route::post('verify-mobile', [ClientAuthController::class, 'verifyMobile'])->name('verify.mobile');
        //manage settings
        Route::post('configuration', [ClientSettingController::class, 'setConfiguration'])->name('setting.store');


    });

//        Route::post('refresh', [ClientAuthController::class, 'refreshToken'])->name('refresh.token');
    // for activation user

    Route::post('logout', [ClientController::class, 'logout'])->name('logout');
    Route::post('login', [ClientController::class, 'login'])->name('login')->middleware('throttle:login');
    Route::post('register', [ClientController::class, 'register'])->name('register');
});
