<?php

use App\Http\Controllers\Api\v1\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Api\v1\Admin\SettingController as AdminSettingController;
use Illuminate\Support\Facades\Route;

//admin routes
Route::group(['middleware' => ['api', 'admin']], function () {

    //    SuperAdmin section
    //        just super admin can manage him Self and Operators
    Route::group(['prefix' => 'admin/auth/', 'as' => 'admin.'], function () {




        Route::get('profile', [AdminAuthController::class, 'profile'])->name('profile');
        Route::put('update', [AdminAuthController::class, 'update'])->name('update');
        Route::post('login', [AdminAuthController::class, 'login'])->name('login')->middleware('throttle:login');
        Route::post('logout', [AdminAuthController::class, 'logout'])->name('logout');

        //    manage settings by superAdmin
        Route::post('configuration/store', [AdminSettingController::class, 'storeConfiguration'])->name('setting.store');
        Route::get('configurations', [AdminSettingController::class, 'index'])->name('setting.index');
        Route::post('login-as-developer', [AdminSettingController::class, 'setLoginAsDeveloper'])->name('as.developer');

        //    for admin and operators

        Route::post('manage/customers', [App\Http\Controllers\Api\v1\Admin\ManageCustomerController::class, 'registeredCustomers'])->name('manage.customers');
        Route::post('customers/inactive', [App\Http\Controllers\Api\v1\Admin\ManageCustomerController::class, 'inactiveCustomers'])->name('customers.inactive');
        Route::post('customer/inactive/user/{id}', [App\Http\Controllers\Api\v1\Admin\ManageCustomerController::class, 'inactiveCustomerUser'])->name('customer.inactive.user');

        //super admin can manage operator
        Route::group(['prefix' => 'operators/', 'as' => 'operators.'], function () {
            Route::get('/', [AdminAuthController::class, 'getAllOperators'])->name('getAll');
            Route::post('register', [AdminAuthController::class, 'registerOperator'])->name('register');
            Route::put('update/{id}', [AdminAuthController::class, 'updateOperator'])->name('update');
            Route::get('/{id}', [AdminAuthController::class, 'getOperator'])->name('getSingle');
            Route::delete('{id}', [AdminAuthController::class, 'deleteOperator'])->name('remove');
        });

    });

//    Operator section
    //    the operator can manage own account
    Route::group(['middleware' => ['operator'], 'prefix' => 'operator/auth/', 'as' => 'operator.'], function () {
        Route::put('update', [App\Http\Controllers\Api\v1\Admin\OperatorController::class, 'update'])->name('selfUpdate');
        Route::post('profile', [App\Http\Controllers\Api\v1\Admin\OperatorController::class, 'profile'])->name('profile');
    });
});
