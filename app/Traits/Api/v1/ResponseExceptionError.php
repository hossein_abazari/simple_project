<?php

namespace App\Traits\Api\v1;

use App\Http\Resources\Admins\ManageCustomerCollection;
use App\Services\ErrorActionsService;
use App\Services\Logs\LogsActions;
use App\Services\User\UserPropertyService;
use Illuminate\Http\JsonResponse;
use Throwable;

trait ResponseExceptionError
{
    /**
     * @param Throwable $e
     * @param int $code
     * @return JsonResponse
     */
    public static function showError(Throwable $e, int $code): JsonResponse
    {
        if (request()->hasHeader('device-type') && request()->header('device-type')) {
            return ErrorActionsService::reWriteExceptionMobiles($e, $code);
        } else {
            if (UserPropertyService::canWatchFullExceptions()) { // if user is developer or debug is true can see errors with details.
                return ErrorActionsService::reWriteExceptionDebugMode($e);
            }

            return ErrorActionsService::reWriteExceptionCustomers($e);
        }
    }

    public static function handleError(Throwable $exception, int $code = 0): JsonResponse
    {
        // log errors for debug.
        if (! app()->runningUnitTests()) {
            LogsActions::logErrors($exception);
        }
        //user or admin can see errors as json response.
        return self::showError($exception, $code);
    }
}
