<?php

namespace App\Traits\Api\v1;

use Illuminate\Http\JsonResponse;

trait Responsible
{
    /**
     * Core of response
     *
     * @param string $message
     * @param array|object $data
     * @param int $statusCode
     * @param bool $isSuccess
     * @param $code
     * @return JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function coreResponse(string $message, $data, int $statusCode, bool $isSuccess, $code): JsonResponse
    {
        $device_type = request()->header('Device-Type');
        if ($device_type == 'mobile') {
            if (! $message) {
                return response()->json(['message' => 'Message is required'], 500);
            }

            if ($isSuccess) {
                return response()->json([
                    'message' => $message,
                    'error' => false,
                    'status' => $statusCode,
                    'code' => $code,
                    'results' => $data,
                ], $statusCode);
            }

            return response()->json([
                'message' => $message,
                'error' => true,
                'status' => $statusCode,
                'code' => $code,
            ], $statusCode);
        } else {

            // Check the params
            if (! $message) {
                return response()->json(['message' => 'Message is required'], 500);
            }
            // Send the response
            if ($isSuccess) {
                return response()->json([
                    'message' => $message,
                    'error' => false,
                    'status' => $statusCode,
                    'citizenship' => config()->get('globals.customer.citizenship'),
                    'max_upload_size' => config()->get('globals.upload_size'),
                    'results' => $data,
                ], $statusCode);
            }

            return response()->json([
                'message' => $message,
                'error' => true,
                'status' => $statusCode,
            ], $statusCode);
        }
    }

    /**
     * @param string $message
     * @param array $data
     * @param int $statusCode
     * @param bool $isSuccess
     * @param int $code
     * @return JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function success(string $message, $data = [], int $statusCode = 200, bool $isSuccess = true, int $code = 0): JsonResponse
    {
        return $this->coreResponse($message, $data, $statusCode, $isSuccess, $code);
    }

    /**
     * @param string $message
     * @param int $statusCode
     * @param bool $isSuccess
     * @param int $code
     * @return JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function error(string $message, int $statusCode = 500, bool $isSuccess = false, int $code = 0): JsonResponse
    {
        return $this->coreResponse($message, null, $statusCode, $isSuccess, false, $code);
    }
}
