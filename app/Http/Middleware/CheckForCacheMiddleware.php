<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckForCacheMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        TODO: this will complete for active or not active cache
//        if (config('globals.cache.status')){
//
//        }
        return $this->error('check for cache middleware', Response::HTTP_FORBIDDEN);
//        return $next($request);
    }
}
