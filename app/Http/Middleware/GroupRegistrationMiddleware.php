<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GroupRegistrationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $group = $request->only('group');
//        Todo: if there is a group code, this will be added when the user registers

        return $response;
    }
}
