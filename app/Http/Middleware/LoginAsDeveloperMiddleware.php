<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginAsDeveloperMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        session()->put('developer', false);

        if (! $this->isEnableDevelopMode()) {
            return $next($request);
        }
//            $credentialMark is ips or user_agents
        $credentialMark = $this->credentialType(); // ip or userAgent
        //  ips or user_agents get from config file
        $credentials = $this->credentialTypeOptions($credentialMark);
        //            get user IPs Or userAgents
        $idSwitchType = $this->checkOptions($credentialMark, $request);

        $developerIdentify = [];
//            set user $credential detail
        array_push($developerIdentify, $idSwitchType);
//        check for
        $user_exist = $this->userIsDeveloper($credentials, $developerIdentify);
        ($user_exist) ? session()->put('developer', true) : session()->put('developer', false);

        return $next($request);
    }

    private function isEnableDevelopMode(): mixed
    {
        return config('globals.develop_mode.status');
    }

    private function credentialType(): mixed
    {
        return config()->get('globals.develop_mode.developer_identification');
    }

    private function credentialTypeOptions(mixed $credentialMark): \Illuminate\Support\Collection
    {
        return collect(config()->get('globals.develop_identifications.'.$credentialMark));
    }

    private function checkOptions(mixed $credentialMark, Request $request): string|array|null
    {
        return ($credentialMark == 'ips') ? $request->ip() :
            (($credentialMark == 'user_agents') ? $request->header('User-Agent') :
                $request->ip());
    }

    public function userIsDeveloper($credentials, $developerIdentify)
    {
        return $credentials->contains(function ($value, $key) use ($developerIdentify) {
            return in_array($value, $developerIdentify);
        });
    }
}
