<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IranianUserMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->guard('user')->check() && (config('globals.customer.citizenship') === config('globals.customer.iranian'))) {
            return $next($request);
        }

        return $this->error(__('user.is_not_iranian'), Response::HTTP_FORBIDDEN);
    }
}
