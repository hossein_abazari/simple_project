<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $langType =
            $request->query('locale') ??
            $request->route()->parameter('locale') ??
            $request->header('APP-LANG') ??
            $request->app_lang ??
            app()->getLocale();

        app()->setLocale($langType);

//        remove local parameter
        if ($request->route()->parameter('locale')) {
            $request->route()->forgetParameter('locale');
        }

        $response = $next($request);
        // set Content Languages header in the response
        $response->headers->set('Content-Language', app()->getLocale());

        return $response;
    }
}
