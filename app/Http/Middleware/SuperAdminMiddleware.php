<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SuperAdminMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->guard('admin')->check() && auth()->guard('admin')->user()->isSuperAdmin()) {
            return $next($request);
        }
        if (auth()->guard('admin')->check()) {
            auth()->guard('admin')->user()->tokens()->delete();
        }

        return $this->error(__('admin.dont_access'), Response::HTTP_UNAUTHORIZED);
    }
}
