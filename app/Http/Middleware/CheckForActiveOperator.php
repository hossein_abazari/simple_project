<?php

namespace App\Http\Middleware;

use App\Http\Resources\Admins\LoginAnswerCollection;
use App\Models\LoginQuestion;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckForActiveOperator
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! (auth()->guard('admin')->check() && ! auth()->guard('admin')->user()->isSuperAdmin() && ! auth()->guard('admin')->user()->isActiveOperator())) {
            return $next($request);
        }
        $success = [
            'questions' => new LoginAnswerCollection(LoginQuestion::all()),
        ];

        return $this->success(__('admin.operator.questions'), $success, Response::HTTP_FORBIDDEN);
    }
}
