<?php

namespace App\Http\Middleware;

use App\Services\RouteService;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserGeneralInfoMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $nonSensitive = [
            'client.two-factor-auth.verify',
            'client.two-factor-auth.resend',
        ];

        if (RouteService::unExpectedRoutes($nonSensitive, $request->route()->getName()) && ! auth()->guard('user')->user()->profile()->exists()) {
            return $this->error(__('user.has_not_general_info'), Response::HTTP_FORBIDDEN);
        } else {
            return $next($request);
        }
    }
}
