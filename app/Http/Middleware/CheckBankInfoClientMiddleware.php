<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckBankInfoClientMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        this step is checked after entire residency info level
        if (! auth()->guard('user')->user()->hasBankInfo()) {
            return $this->error(__('user.not_bank_info'), Response::HTTP_FORBIDDEN);
        }

        //        user have bank  information
        return $next($request);
    }
}
