<?php

namespace App\Http\Middleware;

use App\Services\RouteService;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckPinCodeMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $sensitive = [
            // widthroal route name
            // deposit route name
        ];

        if (auth()->guard('user')->check() && RouteService::expectedRoutes($sensitive, $request->route()->getName()) && ! auth()->guard('user')->user()->hasPinCode()) {
            return $this->error(__('user.has_not_pincode'), Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
