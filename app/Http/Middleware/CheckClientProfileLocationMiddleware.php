<?php

namespace App\Http\Middleware;

use App\Services\RouteService;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckClientProfileLocationMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $nonSensitive = [
            'client.profile.location.store',
            'client.activate.user',
            'client.activation',
            'client.profile.general.get',
            'client.document.general.get',
            'client.profile',
            'client.profile.general.store',
            'client.logout',
            'client.login',
            'client.register',
        ];

        if (auth()->guard('user')->check() &&
            RouteService::unExpectedRoutes($nonSensitive, $request->route()->getName()) &&
            ! auth()->guard('user')->user()->hasLocation()) {
            return $this->error(__('user.profile.has_not_location'), Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
