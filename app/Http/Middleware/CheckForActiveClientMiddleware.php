<?php

namespace App\Http\Middleware;

use App\Services\RouteService;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckForActiveClientMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $nonSensitive = [
            'client.login',
            'client.register',
            'client.activation',
            'client.activate.mobile',
            'client.logout',
            'client.activate.user',
            'client.profile.general.get',
            'client.document.general.get',
            'client.profile',
            'client.profile.general.store',
        ];

        // user will active after click on activation email
        if (auth()->guard('user')->check() &&
            RouteService::unExpectedRoutes($nonSensitive, $request->route()->getName()) &&
            ! auth()->guard('user')->user()->isActive()) {
            return $this->error(__('user.not_active'), Response::HTTP_FORBIDDEN);
        }

        // user is activated
        return $next($request);
    }
}
