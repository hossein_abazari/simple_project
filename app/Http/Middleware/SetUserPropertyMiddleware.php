<?php

namespace App\Http\Middleware;

use App\Services\User\UserPropertyService;
use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;

class SetUserPropertyMiddleware
{
    use Responsible;

    protected $property;

    protected $user;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //fill user properties
        $this->property = new UserPropertyService();
        if (auth()->guard('sanctum')->check()) {
            $this->user = auth()->guard('sanctum')->user();
            if (auth()->guard('admin')->check() && (auth()->guard('admin')->user()->isSuperAdmin())) {
                $this->property->setAdmin($this->user);
            } elseif (auth()->guard('user')->check()) {
                $this->property->setUser($this->user);
            }
        }

        return $next($request);
    }
}
