<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! headers_sent()) {
            header('Access-Control-Allow-Methods: *');
            header('Access-Control-Allow-Headers: *');
        }

        return $next($request);
    }
}
