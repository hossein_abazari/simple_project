<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ReferralRegistrationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $referral = $request->only('referral');
//        Todo: if there is a referral code, this will be added when the user registers

        return $response;
    }
}
