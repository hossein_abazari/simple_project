<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckResidencyClientMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        this step id checked after entering the user information level
        if (! auth()->guard('user')->user()->hasResidency()) {
            return $this->error(__('user.not_residency'), Response::HTTP_FORBIDDEN);
        }

        //        user have residency information
        return $next($request);
    }
}
