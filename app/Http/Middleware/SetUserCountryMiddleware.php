<?php

namespace App\Http\Middleware;

use App\Services\RouteService;
use Closure;
use Illuminate\Http\Request;

class SetUserCountryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $nonSensitive = [
            'client.profile.location.store',
            //            'client.profile',
            'client.profile.general.store',
            'client.profile.security.store',
            'client.login',
            'client.logout',
            'client.register',
            'client.activate.mobile',
            'client.verify.mobile',
        ];

        if (auth()->guard('user')->check() &&
            RouteService::unExpectedRoutes($nonSensitive, $request->route()->getName()) &&
            auth()->guard('user')->user()->isActive()) {
            $abbreviation = auth()->guard('user')->user()->citizenship()->first() ? auth()->guard('user')->user()->citizenship()->first()->abbreviation : auth()->guard('user')->user()->citizenship;

            config()->set('globals.customer.citizenship', $abbreviation);
        }

        return $next($request);
    }
}
