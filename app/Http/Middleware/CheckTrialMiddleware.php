<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

class CheckTrialMiddleware
{
    use Responsible;

//        TODO: change routes for trail version
    private $routes = [
        '',
    ];

    /**
     * @return int
     */
    private static function isExpiredTime(): int
    {
        $startTime = auth()->guard('user')->user()->created_at;
        $expireTime = Carbon::parse($startTime)->addDays(config('globals.expire_trial')); // per days
        $now = Carbon::now();

        return $expireTime->diffInMicroseconds($now) <= 0;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! auth()->guard('user')->check()//if user not login
            || $this::isUrlMatch($request, $this->routes) //check for is matching route?
            || ! self::isExpiredTime()) {
            return $next($request);
        }
        if (self::isExpiredTime()) {
            auth()->guard('user')->user()->tokens()->delete();

            return $this->error('auth.expired_trial');
        }
    }

    public static function isUrlMatch(Request $request, array $routes): bool
    {
        $route = Route::getRoutes()->match($request);

        return in_array($route->getName(), $routes);
    }
}
