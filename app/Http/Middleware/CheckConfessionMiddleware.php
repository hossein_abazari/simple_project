<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckConfessionMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        this step is checked after entire bank info level
        if (! auth()->guard('user')->user()->hasConfession()) {
            return $this->error(__('user.not_confession'), Response::HTTP_FORBIDDEN);
        }

        //       the user confesses after reading
        return $next($request);
    }
}
