<?php

namespace App\Http\Middleware;

use App\Traits\Api\v1\Responsible;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckIdentifyClientMiddleware
{
    use Responsible;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        this step is checked after activation user level
        if (! auth()->guard('user')->user()->hasBeenIdentified()) {
            return $this->error(__('user.not_identify'), Response::HTTP_FORBIDDEN);
        }

//        user have identification information
        return $next($request);
    }
}
