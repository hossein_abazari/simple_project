<?php

namespace App\Http\Resources\Users;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $city = $this->residency()->first() ? $this->residency()->first()->pivot->city : '';

        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'isActive' => $this->is_active,
            //            'detail' => $this->detail,
            'pre_country_code'=> Country::select('precode', 'abbreviation')->get(),
            'profile' => $this->profile,
            'phone_country_code' => $this->phone_country_code,
            'mobile' => $this->phone,
            'citizenship' => $this->citizenship()->first(),
            'residency' => $this->residency()->first(),
            'city' => $city,
            'twoFactorAuth' => $this->twoFactorAuth,
        ];
    }
}
