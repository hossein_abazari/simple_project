<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\ActivationMobileRequest;
use App\Http\Requests\Customer\ActiveRequest;
use App\Http\Requests\Customer\VerifyMobileRequest;
use App\Repositories\Customers\AuthRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->middleware('auth:user');
        $this->authRepository = $authRepository;
    }

    public function sendActivationUser(): JsonResponse
    {
        $email = \auth()->guard('user')->user()->email;

        $active = $this->authRepository->activationEmail($email);

        if ($active) {
            return $this->success(__('user.active'));
        }

    }

    public function sendActivationMobile(ActivationMobileRequest $request): JsonResponse
    {
        $activationMobile = $this->authRepository->activationMobile($request->all());

        if ($activationMobile) {
            return $this->success(__('user.active'));
        }

    }

    public function verifyMobile(VerifyMobileRequest $request)
    {
        $verifyMobile = $this->authRepository->verifyMobile($request->code);

        return $this->success(__('user.active'), $verifyMobile);
    }

    public function refreshToken(): JsonResponse
    {
        $id = \auth()->guard('user')->id();

        $refreshToken = $this->authRepository->refreshToken($id);

        return $this->success(__('user.refresh_token'), $refreshToken, Response::HTTP_OK);

    }

}
