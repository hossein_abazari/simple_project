<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use App\Repositories\Customers\SettingRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SettingController extends Controller
{
    /**
     * @var SettingRepositoryInterface
     */
    private $settingRepository;

    public function __construct(SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth:user');
        $this->settingRepository = $settingRepository;
    }

    public function setConfiguration(UserSettingRequest $request): JsonResponse
    {
        $userId = auth()->guard('user')->id();

        $configuration = $this->settingRepository->setConfiguration($request, $userId);

        return $this->success(__('setting.store_notification_config'),$configuration , Response::HTTP_OK);

    }
}
