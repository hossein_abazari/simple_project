<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\ActiveRequest;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\Customer\UserRequest;
use App\Repositories\Customers\UsersRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    /**
     * @var UsersRepositoryInterface
     */
    private $userRepository;

    public function __construct(UsersRepositoryInterface $authRepository)
    {
        $this->middleware('auth:user', ['only' => ['logout']]);
        $this->middleware('addReferralToRegister', ['only' => ['register']]);
        $this->middleware('addGroupToRegister', ['only' => ['register']]);

        $this->userRepository = $authRepository;
    }

    public function register(UserRequest $request): JsonResponse
    {
        $register = $this->userRepository->register($request);

        return $this->success(__('user.register.notactive'), $register, Response::HTTP_CREATED);

    }

    public function login(LoginRequest $request)
    {
        $result = $this->userRepository->login($request);

        return $this->success(__($result['responseMsg']), $result , Response::HTTP_OK)->withCookie('access_token', $result['token'], 8000, null, null, true, true);

    }

    public function logout(): JsonResponse
    {
        $useId = auth()->guard('user')->id();

        $logout = $this->userRepository->logout($useId);

        if ($logout) {

            return $this->success(__('user.auth.logout'));

        }

    }

}
