<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ActivationMobileRequest;
use App\Http\Requests\Admin\AdminAuthRequest;
use App\Http\Requests\Admin\AdminSecurityProfileRequest;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\OperatorAuthRequest;
use App\Http\Requests\Admin\PinCodeMobileRequest;
use App\Http\Requests\Admin\ProfileSearchRequest;
use App\Http\Requests\Admin\TwoFactorAuthRequest;
use App\Http\Requests\Customer\AdminGeneralProfileRequest;
use App\Http\Requests\Customer\ForeignProfileRequest;
use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use App\Http\Resources\Admins\OperatorResource;
use App\Http\Resources\Admins\OperatorsCollection;
use App\Http\Resources\Admins\UserResource;
use App\Repositories\Admins\AuthRepositoryInterface;
use App\Traits\Api\v1\Responsible;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AuthController extends Controller
{
    use Responsible;
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->middleware('auth:admin', ['except' => ['login', 'changeLanguage']]);
        $this->middleware('super_admin', ['except' => ['login', 'changeLanguage', 'logout']]);
        $this->authRepository = $authRepository;
    }

    public function registerOperator(OperatorAuthRequest $request): JsonResponse
    {
        $success = $this->authRepository->registerOperator($request);

        return $this->success(__('admin.operator.register'), $success, Response::HTTP_OK);

    }

    public function getAllOperators(): JsonResponse
    {
        $operators = $this->authRepository->getAllOperators();
        return $this->success(__('auth.operator.getAll'), $operators, Response::HTTP_OK);

    }

    public function updateOperator(OperatorAuthRequest $request, $id): JsonResponse
    {
        $request->merge(['id' => $id]);

        $operator = $this->authRepository->updateOperator($request, $id);

        return $this->success(__('auth.operator.update'), $operator, Response::HTTP_OK);

    }

    public function getOperator($id): JsonResponse
    {
        $operator = $this->authRepository->getOperator($id);

        return $this->success(__('auth.operator.getSingle'), $operator, Response::HTTP_OK);

    }

    public function update(AdminAuthRequest $request): JsonResponse
    {
        $id = auth()->guard('admin')->id();

        $adminAuth =  $this->authRepository->update($request, $id);

        return $this->success(__('auth.admin.update'), $adminAuth , Response::HTTP_OK);
    }

    public function login(LoginRequest $request)
    {
        $login = $this->authRepository->login($request);

        return $this->success(__('admin.operator.questions_login'), $login);
    }

    public function profile(): JsonResponse
    {
        $id = \auth()->guard('admin')->id();

        $profile = $this->authRepository->profile($id);

        return $this->success(__('auth.admin.details'), $profile);

    }

    public function deleteOperator($id): JsonResponse
    {
        $operator = $this->authRepository->destroy($id);

        return $this->success(__('auth.admin.delete'), $operator);

    }

    public function logout()
    {
        $id = \auth()->guard('admin')->id();

        $logout = $this->authRepository->logout($id);
        return $this->success(__($responseMessage),$logout);

    }

}
