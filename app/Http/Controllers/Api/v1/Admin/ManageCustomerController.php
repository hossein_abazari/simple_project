<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ManageCustomerRequest;
use App\Http\Resources\Admins\ManageCustomerCollection;
use App\Repositories\Admins\Eloquent\ManageCustomerRepository;
use App\Repositories\Admins\ManageCustomerRepositoryInterface;
use App\Traits\Api\v1\Responsible;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kashefy\Modules\ModuleManagement\src\Services\Responses\JsonResponse;

class ManageCustomerController extends Controller
{
    use Responsible;

    protected $manageCustomerRepository;

    /**
     * Set Repository for controller
     *
     * @param ManageCustomerRepository $managerCustomerRepository
     */
    public function __construct(ManageCustomerRepositoryInterface $managerCustomerRepository)
    {
        $this->manageCustomerRepository = $managerCustomerRepository;
    }

    /**
     * get find new registered customer in interface
     *
     * @param ManageCustomerRequest $request
     * @return void
     */
    public function registeredCustomers(ManageCustomerRequest $request)
    {
        $manageCustomer = $this->manageCustomerRepository->getRegisteredCustomers($request);

        return $this->success(__('admin.customer.registered'), $manageCustomer, Response::HTTP_OK);

    }

    /**
     * Inactive Customers
     *
     * @return void
     */
    public function inactiveCustomers()
    {
        $inactiveCustomer = $this->manageCustomerRepository->getInactiveCustomers();

        return $this->success(__('admin.customer.is_active'), $inactiveCustomer , Response::HTTP_OK);

    }

    /**
     * Inactive Customer with id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function inactiveCustomerUser($id)
    {
        $inactiveCustomerUser = $this->manageCustomerRepository->getInactiveCustomerUser($id);

        return $this->success(__('admin.customer.is_active.user'), $inactiveCustomerUser , Response::HTTP_OK);

    }
}
