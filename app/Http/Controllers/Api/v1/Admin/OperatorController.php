<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OperatorSelfAuthRequest;
use App\Http\Resources\Admins\UserResource;
use App\Repositories\Admins\OperatorRepositoryInterface;
use App\Traits\Api\v1\Responsible;
use Illuminate\Http\Response;

class OperatorController extends Controller
{
    use Responsible;

    /**
     * @var operatorRepositoryInterface
     */
    private $operatorRepository;

    /**
     * @param OperatorRepositoryInterface $operatorRepository
     */
    public function __construct(OperatorRepositoryInterface $operatorRepository)
    {
        $this->middleware('auth:admin');
        $this->middleware('is_active_operator');
        $this->operatorRepository = $operatorRepository;
    }

    public function profile()
    {
        $id = auth()->guard('admin')->id();

        $operator = $this->operatorRepository->profile($id);
        return $this->success(__('admin.operator.show'), $operator, 200);

    }

    public function update(OperatorSelfAuthRequest $request)
    {
        $id = auth()->guard('admin')->id();

        $operator = $this->operatorRepository->update($request, $id);

        return $this->success(__('admin.operator.update'), $operator , Response::HTTP_OK);

    }
}
