<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Exceptions\api\v1\QueryNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ConfigRequest;
use App\Http\Requests\Admin\LoginAsDeveloperRequest;
use App\Models\User;
use App\Repositories\Admins\AuthRepositoryInterface;
use App\Repositories\Admins\SettingRepositoryInterface;
use App\Traits\Api\v1\Responsible;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SettingController extends Controller
{
    use Responsible;

    /**
     * @var AuthRepositoryInterface
     */
    private $settingRepository;

    public function __construct(SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth:admin');
        $this->middleware('super_admin');
        $this->settingRepository = $settingRepository;
    }

    public function setLoginAsDeveloper(LoginAsDeveloperRequest $request)
    {
        $setLoginAsDeveloper = $this->settingRepository->setLoginAsDeveloper($request);

        return $this->success(__('auth.set_login_as_developer'), $setLoginAsDeveloper, Response::HTTP_OK);

    }

    public function storeConfiguration(Request $request): JsonResponse
    {
        $configuration = $this->settingRepository->storeConfiguration($request);
        return $this->success(__('setting.store_settings'), $configuration, Response::HTTP_OK);

    }

    public function index(): JsonResponse
    {
        $setting = $this->settingRepository->index();

        return $this->success(__('setting.get_settings'), $setting , Response::HTTP_OK);

    }
}
