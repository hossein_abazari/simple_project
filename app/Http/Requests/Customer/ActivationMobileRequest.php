<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class ActivationMobileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth()->guard('user')->user()->mobile ? auth()->guard('user')->user()->mobile->id : null;

        return [
            'mobile' => "required|regex:/^\+?\d+$/|unique:mobiles,mobile,$id",
            'mobile_country_code' => 'required',
        ];
    }
}
