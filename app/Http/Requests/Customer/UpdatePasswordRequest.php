<?php

namespace App\Http\Requests\Customer;

use App\Rules\ResetPasswordTokenExpired;
use App\Rules\StrongCharacter;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'token' => ['required', new ResetPasswordTokenExpired()],
            'email' => 'required|email|exists:users,email',
            'password' => ['required', 'unique:users,password', 'confirmed', 'min:8', 'max:16', new StrongCharacter()],
        ];
    }
}
