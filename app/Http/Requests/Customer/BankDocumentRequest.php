<?php

namespace App\Http\Requests\Customer;

use App\Models\DocumentBank;
use App\Rules\CheckInArray;
use App\Services\Documents\DocumentService;
use App\Services\IdentificationService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BankDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = auth()->id();
        $bank_number = ['603799', '589210', '627648', '627961', '603770', '628023', '627760', '502908', '627412', '622106', '627884', '639194', '502229', '639347', '639599', '627488', '621986', '639346', '639607', '504706', '502806', '502938', '603769', '610433', '991975', '627353', '585983', '589463', '627381', '505785', '636214', '636949', '639370'];
//            performs calculations
        DocumentService::isPending($userId, DocumentBank::class);
        DocumentService::isRejected($userId, DocumentBank::class);

        return [
            'numbers' => ['required', new CheckInArray($bank_number), "regex:/^[0-9\s]+$/"],
            'file_proof' => [Rule::when(IdentificationService::isIranian($userId), 'required'), 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            'iban' => ['required', "regex:/^[a-zA-Z0-9\s]+$/"],
            'expiration_date' => 'required|date',
        ];
    }
}
