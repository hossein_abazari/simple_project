<?php

namespace App\Http\Requests\Customer;

use App\Rules\EnglishCharacter;
use App\Rules\StrongCharacter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id = auth()->guard('user')->id(); // id{integer} of super admin

        return [
            'email' => [
                Rule::when(request()->isMethod('PUT'), 'required'),
                'string', 'email', 'max:30',
                Rule::when(request()->isMethod('PUT'), 'unique:users,email,'.$id), //this id is from user update
                Rule::when(request()->isMethod('POST'), 'unique:users'),
            ],
            'first_name' => ['required', 'max:50', new EnglishCharacter],
            'last_name' => ['required', 'max:50', new EnglishCharacter],
            'language' => [
                Rule::when(request()->isMethod('POST'), ['required', 'in:fa,en,ar,cn,cs,de,pt,ru']),
            ],
            'password' => ['required', 'unique:users,password', 'confirmed', 'min:8', 'max:16', new StrongCharacter()],
            'referral' => [Rule::when(request()->has('referral'), 'required')],
            'group' => [Rule::when(request()->has('group'), 'required')],
            //            Todo: uncomment recaptcha on production
            //            'recaptcha' => 'recaptcha',
        ];
    }
}
