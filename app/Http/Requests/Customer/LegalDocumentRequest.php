<?php

namespace App\Http\Requests\Customer;

use App\Models\DocumentLegal;
use App\Rules\EnglishPersianCharacter;
use App\Services\Documents\DocumentService;
use Illuminate\Foundation\Http\FormRequest;

class LegalDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = auth()->id();
        DocumentService::isPending($user_id, DocumentLegal::class);
        DocumentService::isVerified($user_id, DocumentLegal::class);
        DocumentService::isRejected($user_id, DocumentLegal::class);

        return [
            'company_name' => ['required', new EnglishPersianCharacter],
            'company_national_id' => ['required', 'integer'],
            'registration_city' => ['required', new EnglishPersianCharacter],
            'registration_date' => 'required|date',
            'company_type_id' => 'required|integer',
            'legal_representative_name' => ['required', new EnglishPersianCharacter],
            'file_proof' => ['required', 'array'],
            'file_proof.*' => ['required', 'mimes:png,jpg,jpeg.pdf', 'max:'.config('globals.upload_size')],
        ];
    }
}
