<?php

namespace App\Http\Requests\Customer;

use App\Models\DocumentResidency;
use App\Services\Documents\DocumentService;
use Illuminate\Foundation\Http\FormRequest;

class ResidencyDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return \string[][]
     * @throws \Throwable
     */
    public function rules()
    {
        $userId = auth()->id();
        DocumentService::isPending($userId, DocumentResidency::class);
        DocumentService::isVerified($userId, DocumentResidency::class);
        DocumentService::isRejected($userId, DocumentResidency::class);

        return [
            'file_proof' => ['required', 'mimes:png,jpg,jpeg,pdf', 'max:'.config('globals.upload_size')],
            'address' => ['required'],
        ];
    }
}
