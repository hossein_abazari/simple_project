<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class AdminGeneralProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [];

        return $rule + [
            'city' => 'required|string',
            'citizenship_id' => 'required|string',
            'residency_id' => 'required|string',
            'email' => 'required|string',
            'first_name' => 'required|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|regex:/^[a-zA-Z]+$/u',
            'phone_country_code' => 'nullable',
            'phone' => 'required|regex:/[0-9]/|min:3|max:15',
            'experience' => ['required', 'in:None,1-3 Year(s),3-5 Year(s),5-10 Year(s),10 Or More Year(s)'],
            'gender' => ['required', 'in:Male,Female'],
            'hear' => ['required', 'in:Others,YouTube,Website/Search Engine,Twitter,TV/Cable News,Newspaper Story,Magazine Article,Family or Friend,Facebook,Email/Newsletter,Advertisement'],
            'hear_desc' => 'sometimes|required',
            'birth_date' => 'required|date',
            'profession' => 'nullable|string|max:50',
            'trade_style' => ['nullable', 'in:Scalping,Day Trading,Swinging,A combination of the above'],
            'trade_asset' => ['nullable', 'in:Commodities,Currencies,Shares,Indices'],
        ];
    }
}
