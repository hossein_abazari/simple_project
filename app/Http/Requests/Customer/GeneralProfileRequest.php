<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class GeneralProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $citizenship = \auth()->guard('user')->user()->citizenship()->first();
        $residency = \auth()->guard('user')->user()->residency()->first();
        $rule = [];
        if (! $residency || ! $citizenship) {
            $rule = [
                'residency_id' => 'required|integer|min:1|max:200',
                'citizenship_id' => 'required|integer|min:1|max:200',
            ];
        }

        return $rule + [
            'city' => 'required|string',
            'first_name' => 'required|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|regex:/^[a-zA-Z]+$/u',
            'experience_id' => ['required', 'integer', 'min:1', 'max:30'],
            'gender_id' => ['required', 'integer', 'min:1', 'max:3'],
            'hear_id' => ['required', 'integer', 'min:1', 'max:30'],
            'hear_desc' => 'sometimes|required',
            'birth_date' => 'required|date',
            'profession' => 'nullable|string|max:50',
            'trade_style_id' => ['nullable', 'integer', 'min:1', 'max:25'],
            'trade_asset_id' => ['nullable', 'integer', 'min:1', 'max:25'],
        ];
    }
}
