<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class LocationProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $citizenship = \auth()->guard('user')->user()->citizenship()->first();
        $residency = \auth()->guard('user')->user()->residency()->first();
        $rule = [];
        if (! $residency || ! $citizenship) {
            $rule = [
                'residency_id' => 'required|integer|min:1',
                'citizenship_id' => 'required|integer|min:1',
            ];
        }

        return $rule + ['city' => 'required|string'];
    }
}
