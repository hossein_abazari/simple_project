<?php

namespace App\Http\Requests\Customer;

use App\Models\DocumentGeneral;
use App\Services\Documents\DocumentService;
use App\Services\IdentificationService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GeneralDocumentRequest extends FormRequest
{
//    use Responsible;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \Throwable
     */
    public function rules()
    {
        $userId = auth()->id();
        DocumentService::isPending($userId, DocumentGeneral::class);
        DocumentService::isVerified($userId, DocumentGeneral::class);
        DocumentService::isRejected($userId, DocumentGeneral::class);

        return [
            //            'identity_type_id' => [Rule::when(! IdentificationService::isIranian($userId), 'required'), 'integer'],
            'identity_type_id' => ['required', 'integer'],
            //            'file_back' => [Rule::when(IdentificationService::isIranian($userId), 'required'), 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            //            'file_front' => [Rule::when(IdentificationService::isIranian($userId), 'required'), 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            'file_front' => ['required', 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            //            'file_proof' => [Rule::when(! IdentificationService::isIranian($userId), 'required'), 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            'file_proof' => ['required', 'mimes:png,jpg,jpeg', 'max:'.config('globals.upload_size')],
            'id_number' => ['required', Rule::when(! IdentificationService::isIranian($userId), "regex:/^[a-zA-Z0-9\s]+$/"), Rule::when(IdentificationService::isIranian($userId), "regex:/^[0-9\s]+$/")],
            'id_expiration_date' => 'required|date',
        ];
    }
}
