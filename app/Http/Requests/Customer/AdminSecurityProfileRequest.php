<?php

namespace App\Http\Requests\Customer;

use App\Rules\StrongCharacter;
use Illuminate\Foundation\Http\FormRequest;

class AdminSecurityProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'old_password' => ['required', new \App\Rules\AdminSecurityProfile($this->id)],
            'password' => ['required', 'unique:users,password', 'confirmed', 'min:8', 'max:16', new StrongCharacter(), 'different:old_password'],
        ];
    }
}
