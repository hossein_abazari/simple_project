<?php

namespace App\Http\Requests\Admin;

use App\Rules\StrongCharacter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id = auth()->guard('admin')->id(); // super admin id

        return [
            'name' => ['required', 'max:50'],
            'email' => [
                'required',
                'string', 'email', 'max:255',
                Rule::when(request()->isMethod('PUT'), 'unique:admins,email,'.$id),
                Rule::when(request()->isMethod('POST'), 'unique:admins'),
            ],
            'password' => ['required', 'unique:admins,password', 'confirmed', 'min:8', 'max:16', new StrongCharacter()],
        ];
    }
}
