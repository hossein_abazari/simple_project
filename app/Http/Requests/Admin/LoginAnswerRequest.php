<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LoginAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data' => ['required', 'min:3', 'max:3'],
            'data.*' => ['required', 'array', 'min:1'],
            'data.*.question_id' => ['required', 'integer', 'max:999'],
            'data.*.answer' => ['required', 'min:1', 'max:200'],
        ];
    }
}
