<?php

namespace App\Http\Requests\Admin;

use App\Rules\CheckOperatorAnswers;
use App\Rules\HasOperatorAnswers;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $answers = $this->answers ?? [];
        $email = $this->email;

        return [
            'email' => 'bail|required|email|exists:admins,email',
            'type' => ['required', 'in:email,question'],
            'answers' => ['required_if:type,question', 'bail', 'sometimes', 'required', 'min:3', 'max:3', Rule::when($this->type == 'question', [new HasOperatorAnswers($email)]), Rule::when(! empty($this->answers) && $this->answers == 'question', [new CheckOperatorAnswers($answers, $email)])],
            'answers.*' => ['sometimes', 'required', 'array', 'min:1'],
            'answers.*.question_id' => ['sometimes', 'required', 'integer', 'max:999'],
            'answers.*.answer' => ['sometimes', 'required', 'min:1', 'max:200'],
        ];
    }
}
