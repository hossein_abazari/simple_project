<?php

namespace App\Http\Requests\Admin;

use App\Rules\IsUniqueProperty;
use App\Rules\StrongCharacter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OperatorAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $operatorId = request()->id;

        return [
            'name' => ['required', 'max:50'],
            'email' => [
                'required',
                'string', 'email', 'max:255',
                Rule::when(request()->isMethod('PUT'), 'unique:admins,email,'.$operatorId),
                Rule::when(request()->isMethod('POST'), 'unique:admins'),
            ],
            'password' => [
                Rule::when(request()->isMethod('POST'), 'required'),
                Rule::when(request()->isMethod('PUT'), 'sometimes'),
                'unique:admins,password', 'confirmed', 'min:8', 'max:16', new StrongCharacter(), ],
            'role_id' => [Rule::when(request()->isMethod('POST'), 'required'),
                Rule::when(request()->isMethod('PUT'), ['required', new IsUniqueProperty('id', 'roles')]), ],
        ];
    }
}
