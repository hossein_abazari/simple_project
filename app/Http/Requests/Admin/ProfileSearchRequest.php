<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProfileSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['sometimes', 'max:50'],
            'last_name' => ['sometimes', 'max:50'],
            'language' => ['sometimes', 'max:50'],
            'is_active' => ['sometimes', 'max:50'],
            'status' => ['sometimes', 'max:50'],
            'email' => [
                'sometimes',
                'string', 'email', 'max:255',
            ],
            'mt4_account' => ['sometimes', 'max:50'],
            'mt4_group' => ['sometimes', 'max:50'],
            'referral_code' => ['sometimes', 'max:50'],
            'registration_time_from' => ['sometimes', 'date', 'max:50'],
            'registration_time_to' => ['sometimes', 'date', 'max:50'],
        ];
    }
}
