<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->validator);
        return [
            'develop_mode_ips' => 'sometimes|array',
            'develop_mode_user_agents' => 'sometimes|array',
            //            'developer_credential_switch' => 'sometimes|in:ips,user_agents',
            'developer_credential_switch' => 'sometimes|array',
            'language_basic' => 'sometimes|array',
            'timezone' => 'sometimes|array',
            //            'caching' => 'sometimes|in:none,file,redis',
            'caching' => 'sometimes|array',
            'cache_time' => 'sometimes|array',
            'login_limiter_max' => 'sometimes|array',
            'login_limiter_decay' => 'sometimes|array',
            'logs_count' => 'sometimes|array',
            'reset_pwd_time_exp' => 'sometimes|array',
            'expire_trial' => 'sometimes|array',
            'upload_size' => 'sometimes|array',
            'file_mime_type' => 'sometimes|array',
            'two_factor_exp' => 'sometimes|array',
            'default_base_url' => 'sometimes|array',
        ];
    }
}
