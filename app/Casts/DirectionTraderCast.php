<?php

namespace Kashefy\Modules\OmsModule\src\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class DirectionTraderCast implements CastsAttributes
{
    /**
     * Get for Show Data
     *
     * @var string[]
     */
    private $sideGetDirection = [
        'B' => 'buy',
        'S' => 'sell',
        'buy' => 'buy',
        'sell' => 'sell',
    ];

    /**
     * Set For save Data
     *
     * @var string[]
     */
    private $sideSetDirection = [
        'B' => 'B',
        'S' => 'S',
        'buy' => 'B',
        'sell' => 'S',
    ];

    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return trans('settings.traderRequest.'.$this->sideGetDirection[$value]);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return $this->sideSetDirection[$value];
    }
}
