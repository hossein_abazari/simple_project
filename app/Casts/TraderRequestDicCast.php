<?php

namespace Kashefy\Modules\OmsModule\src\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Kashefy\Modules\OmsModule\src\Models\DicTrdreqStatus;
use Kashefy\Modules\OmsModule\src\Models\DicTrdreqType;
use Kashefy\Modules\OmsModule\src\Services\TraderRequestService;

class TraderRequestDicCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        if ($value != null && $key == 'type') {
            $type = TraderRequestService::GetTraderRequestArrayService(DicTrdreqType::class, 'dic_TrdReq_type_id', 'name');

            return $type[$value];
        }
        elseif ($value != null && $key == 'status') {
            $type = TraderRequestService::GetTraderRequestArrayService(DicTrdreqStatus::class, 'dic_TrdReq_status_id', 'name');
            return $type[$value];
        }

        return $value;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return $value;
    }
}
