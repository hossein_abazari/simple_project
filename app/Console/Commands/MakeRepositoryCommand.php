<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Pluralizer;

class MakeRepositoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {--type=customer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make repository interface class';

    /**
     * Filesystem instance
     * @var Filesystem
     */
    protected Filesystem $files;

    protected array $entities = ['Repository', 'Interface'];

    private string $repoBasePath;

    private string $interfaceBasePath;

    private string $className;

    private string $repoNameSpace;

    private string $interfaceNameSpace;

    private string $type;

    private string $repoFileName;

    private string $interfaceFileName;

    private string $repoClassName;

    private string $interfaceClassName;

    private string $serviceProviderFileName;

    private string $insertArrayContent;

    private string $contentType;

    private string $Rtype;

    private string $Rname;

    /**
     * Create a new command instance.
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        $this->serviceProviderFileName = base_path('app'.DIRECTORY_SEPARATOR.'Providers'.DIRECTORY_SEPARATOR.'RepositoryServiceProvider.php');
        $this->files = $files;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (! $this->confirm('do you want continue?', true)) {
            $this->alert('bye');

            return false;
        }
        $this->Rtype = $this->choice(
            'What is your repository type?',
            ['Admins', 'Customers'],
            'Admins'
        );
        $Rname = $this->ask('What is your repository name? [like=> auth]');
        if (empty($Rname)) {
            $this->alert('name is require!');

            return false;
        }
        $this->Rname = $Rname;

//        make files for entities; repository , interface
        $this->makeFile($this->entities);

        //        add to repository and bind class
        if ($this->Rtype == 'Admins') {
            $this->repoNameSpace = 'use '.$this->repoNameSpace.';';
            $this->interfaceNameSpace = 'use '.$this->interfaceNameSpace.';';
            $this->contentType = 'private static array $adminBinds = [';
            $this->insertArrayContent = '['.$this->interfaceClassName.'::class => '.$this->repoClassName.'::class],';
        } elseif ($this->Rtype == 'Customers') {
            $this->repoNameSpace = 'use '.$this->repoNameSpace.' as Client'.$this->repoClassName.';';
            $this->interfaceNameSpace = 'use '.$this->interfaceNameSpace.' as Client'.$this->interfaceClassName.';';
            $this->contentType = 'private static array $clientBinds = [';
            $this->insertArrayContent = '[Client'.$this->interfaceClassName.'::class => Client'.$this->repoClassName.'::class],';
        }
        $this->insertContent($this->serviceProviderFileName, $this->contentType, $this->insertArrayContent, 'use Illuminate\Support\ServiceProvider', [$this->repoNameSpace, $this->interfaceNameSpace]);
    }

    /**
     * Return the Singular Capitalize Name
     * @param $name
     * @return string
     */
    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }

    /**
     * Return the Singular Capitalize Type
     * @param $type
     * @return string
     */
    public function getPluralClassType($type)
    {
        return ucwords(Pluralizer::plural($type));
    }

    /**
     * Return the stub file path
     * @return string
     */
    public function getStubPath($entity)
    {
        if ($entity == 'Repository') {
            return __DIR__.'/../../../stubs/model.repository.stub';
        } elseif ($entity == 'Interface') {
            return __DIR__.'/../../../stubs/model.repository.interface.stub';
        }
    }

    /**
     **
     * Map the stub variables present in stub to its value
     *
     * @return array
     */
    public function getStubVariables($entity)
    {
        if ($entity == 'Repository') {
            return [
                'NAMESPACE' => $this->repoBasePath,
                'CLASS_NAME' => $this->className,
                'TYPE' => $this->Rtype,
            ];
        } elseif ($entity == 'Interface') {
            return [
                'NAMESPACE' => $this->interfaceBasePath,
                'CLASS_NAME' => $this->className,
                'TYPE' => $this->Rtype,
            ];
        } else {
            return [];
        }
    }

    /**
     * Get the stub path and the stub variables
     *
     * @return bool|mixed|string
     */
    public function getSourceFile($entity)
    {
        return $this->getStubContents($this->getStubPath($entity), $this->getStubVariables($entity));
    }

    /**
     * Replace the stub variables(key) with the desire value
     *
     * @param $stub
     * @param array $stubVariables
     * @return bool|mixed|string
     */
    public function getStubContents($stub, $stubVariables = [])
    {
        $contents = file_get_contents($stub);

        foreach ($stubVariables as $search => $replace) {
            $contents = str_replace('$'.$search.'$', $replace, $contents);
        }

        return $contents;
    }

    /**
     * Get the full path of generate class
     *
     * @return string
     */
    public function getSourceFilePath($entity)
    {
        $type = $this->Rtype ?? $this->getPluralClassType($this->option('type'));

        if ($entity == 'Repository') {
            $basePath = 'App'.DIRECTORY_SEPARATOR.'Repositories'.DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.'Eloquent';

            return base_path($basePath).DIRECTORY_SEPARATOR.$this->repoFileName;
        } elseif ($entity == 'Interface') {
            $basePath = 'App'.DIRECTORY_SEPARATOR.'Repositories'.DIRECTORY_SEPARATOR.$type;

            return base_path($basePath).DIRECTORY_SEPARATOR.$this->interfaceFileName;
        }
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true, true);
        }

        return $path;
    }

    private function insertContent(string $file_name, $search_type, $classe_names, $name_space_search, $name_spaces = [])
    {
        $content = file($file_name); //Read the file into an array. Line number => line content

        foreach ($content as $lineNumber => &$lineContent) { //Loop through the array (the "lines")
//            insert bind array
            if (str_contains($lineContent, $name_space_search)) {
                $lineContent .= $name_spaces[0].PHP_EOL;
                $lineContent .= $name_spaces[1].PHP_EOL;
            }

            if (strpos($lineContent, $search_type)) {
                $lineContent .= $classe_names.PHP_EOL;
            }
        }

        $allContent = implode('', $content); //Put the array back into one string
        file_put_contents($file_name, $allContent); //Overwrite the file with the new content
    }

    private function setVariables($entity)
    {
        $this->Rtype = $this->Rtype ?? $this->getPluralClassType($this->option('type')); //Customers or Admins
//        $this->className = $this->Rname;
        $this->className = ucwords(Pluralizer::plural($this->Rname));

        if ($entity == 'Repository') {
            $this->repoBasePath = 'App'.DIRECTORY_SEPARATOR.'Repositories'.DIRECTORY_SEPARATOR.$this->Rtype.DIRECTORY_SEPARATOR.'Eloquent';
            $this->repoFileName = $this->className.'Repository.php';
            $this->repoClassName = $this->className.'Repository';
            $this->repoNameSpace = $this->repoBasePath.DIRECTORY_SEPARATOR.$this->repoClassName;
        }
        if ($entity == 'Interface') {
            $this->interfaceBasePath = 'App'.DIRECTORY_SEPARATOR.'Repositories'.DIRECTORY_SEPARATOR.$this->Rtype;
            $this->interfaceFileName = $this->className.'RepositoryInterface.php';
            $this->interfaceClassName = $this->className.'RepositoryInterface';
            $this->interfaceNameSpace = $this->interfaceBasePath.DIRECTORY_SEPARATOR.$this->interfaceClassName;
        }
    }

    private function makeFile($entities): void
    {
        foreach ($entities as $entity) {
            $this->setVariables($entity);
            $path = $this->getSourceFilePath($entity);
            $this->makeDirectory(dirname($path));

            $contents = $this->getSourceFile($entity);

            if (! $this->files->exists($path)) {
                $this->files->put($path, $contents);
                $this->info("File : {$path} created");
            } else {
                $this->info("File : {$path} already exits");
            }
        }
    }
}
