<?php

namespace App\Repositories\Eloquent;

use App\Repositories\EloquentRepositoryInterface;
use App\Traits\Api\v1\ResponseExceptionError;
use App\Traits\Api\v1\Responsible;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentRepositoryInterface
{
    use Responsible, ResponseExceptionError;

    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     * @return Model
     */
//    public function search(int $id): Model
//    {
//        return $this->model->findOrFail($id);
//    }
}
