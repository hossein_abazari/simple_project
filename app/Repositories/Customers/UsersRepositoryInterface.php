<?php

namespace App\Repositories\Customers;

use App\Http\Requests\Customer\ActiveRequest;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\Customer\UserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

interface UsersRepositoryInterface
{
    /**
     * register new customers
     *
     * @param UserRequest $request
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     *
     * @OA\Post (
     *     path="/en/api/v1/user/register?referral=3&group=333",
     *     summary="register new user",
     *     tags={"Cabin: register"},
     *     security={{"apiAuth":{}
     *     }},
     *      @OA\RequestBody (
     *          @OA\JsonContent (
     *              type="object",
     *              @OA\Property (property="first_name", type="string", default="majid"),
     *              @OA\Property (property="last_name", type="string", default="kashefy"),
     *              @OA\Property (property="email", type="string", default="kashefymajid1992@gmail.com"),
     *              @OA\Property (property="password", type="string", default="Pass123!@#"),
     *              @OA\Property (property="password_confirmation", type="string", default="Pass123!@#"),
     *              @OA\Property (property="language", type="string", default="en")
     *          )
     *      ),
     *     @OA\Response(
     *         response=201,
     *          description="successful registered",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function register(UserRequest $request): JsonResponse;

    /**
     * login user
     *
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws ValidationException
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     * @copyright 2021
     *
     * @OA\Post (
     *     path="/en/api/v1/user/login",
     *     summary="login user",
     *     tags={"Cabin: login"},
     *       @OA\RequestBody (
     *          @OA\JsonContent (
     *              type="object",
     *              @OA\Property (property="email", type="string", default="kashefymajid1992@gmail.com"),
     *              @OA\Property (property="password", type="string", default="Pass123!@#"),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     *   )
     */
    public function login(LoginRequest $request): JsonResponse;

    /**
     * logout user with remove its session and token
     *
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     * @copyright 2021
     *
     * @OA\Post (
     *     path="/en/api/v1/user/logout",
     *     summary="logout user",
     *     tags={"Cabin: logout"},
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     *   )
     */
    public function logout(int $id): JsonResponse;

}
