<?php

namespace App\Repositories\Customers;

use Illuminate\Http\JsonResponse;

interface AuthRepositoryInterface
{
    /**
     * send email for activation
     *
     * @method  Post 'api/v1/user/auth/active-user'
     * @param string $user_email
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     *
     * @OA\Post  (
     *     path="/fa/api/v1/user/auth/active-user",
     *     summary="send email",
     *     tags={"Cabin: activationEmail"},
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function activationEmail(string $user_email): JsonResponse;

    /**
     * to confirm the user's mobile number
     *
     * @method  Post 'api/v1/user/auth/verify-mobile'
     * @param $code
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     *
     * @OA\Post  (
     *     path="/fa/api/v1/user/auth/verify-mobile",
     *     summary="Verify mobile number",
     *     tags={"Cabin: verifyMobile"},
     *     @OA\RequestBody (
     *          @OA\JsonContent (
     *              type="object",
     *              @OA\Property (property="code", type="string", default="197068"),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function verifyMobile($code): mixed;

    /**
     * to register the user's mobile number.
     *
     * @method  Post 'api/v1/user/auth/activation-mobile'
     * @param $mobileInfo
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     *
     * @OA\Post  (
     *     path="/fa/api/v1/user/auth/activation-mobile",
     *     summary="register mobile number",
     *     tags={"Cabin: activationMobile"},
     *     @OA\RequestBody (
     *          @OA\JsonContent (
     *              type="object",
     *              @OA\Property (property="mobile", type="string", default="09011804389"),
     *              @OA\Property (property="mobile_country_code", type="string", default="355"),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function activationMobile($mobileInfo): JsonResponse;
}
