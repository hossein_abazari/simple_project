<?php

namespace App\Repositories\Customers\Eloquent;

use App\Exceptions\api\v1\GeneralException;
use App\Http\Requests\Customer\GeneralProfileRequest;
use App\Http\Requests\Customer\UserRequest;
use App\Http\Resources\Users\UserResource;
use App\Models\User;
use App\Repositories\Customers\AuthRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use App\Services\User\ActivateUser;
use App\Services\User\UserPropertyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;
use function Termwind\ValueObjects\truncate;

class AuthRepository extends BaseRepository implements AuthRepositoryInterface
{
    private ActivateUser $activateUser;

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function refreshToken($id): JsonResponse
    {
        try {
            $user = $this->model->findOrFail($id);
            $user->tokens()->delete();
            $token = $user->createToken('user', ['role:client'])->plainTextToken;

            $success = [
                'token' => $token,   // use sanctum laravel for authorization
            ];

            return $success;
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
//
//    public function storeGeneralProfile(GeneralProfileRequest $request, $id): JsonResponse
//    {
//        try {
//            $model = $this->model->findOrFail($id);
//
//            return $this->success(__('user.details'), [UserResource::make($model)]);
//        } catch (\Exception $e) {
//            return $this::handleError($e);
//        }
//    }

    /**
     * @param string $user_email
     * @return JsonResponse
     * @throws Throwable
     */
    public function activationEmail(string $user_email): JsonResponse
    {
        try {
            $user = $this->model->where('email', $user_email)->first();
            throw_if($user->is_active, new GeneralException('user is activated'));

            UserPropertyService::sendEmailForActiveUser($user->email);

            return true;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function verifyMobile($code): JsonResponse
    {
        \auth()->guard('user')->user()->mobile()->whereCode($code)->update(['is_active' => '1']);

        return $code;
    }

    /**
     * @param $mobileInfo
     * @return JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function activationMobile($mobileInfo): JsonResponse
    {
        try {
            $user = \auth()->guard('user')->user();
            $code = rand(100000, 999999);

            $parameters = [
                'mobile' => $mobileInfo['mobile'],
                'mobile_country_code' => $mobileInfo['mobile_country_code'],
                'is_active' => '0',
                'code' => $code,
                'expire_at' => now()->addMinutes(10),
            ];

            $user->mobile ? $user->mobile()->update($parameters) : $user->mobile()->create($parameters);

            $receiverNumber = $mobileInfo['mobile_country_code'].$mobileInfo['mobile'];

            UserPropertyService::sendSmsForActivePhone($receiverNumber, $code);

            return true;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
