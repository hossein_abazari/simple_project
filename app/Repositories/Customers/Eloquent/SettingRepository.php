<?php

namespace App\Repositories\Customers\Eloquent;

use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use App\Http\Resources\Users\SettingCollection;
use App\Models\UserSetting;
use App\Repositories\Customers\SettingRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{
    /**
     * SettingRepository constructor.
     *
     * @param UserSetting $model
     */
    public function __construct(UserSetting $model)
    {
        parent::__construct($model);
    }

    public function setConfiguration(UserSettingRequest $request, int $user_id): JsonResponse
    {
        try {
            foreach ($request->validated() as $k => $settings) {
                $this->model->findOrUpdateOrCreate($k, [
                    'user_id' => $user_id,
                    'key' => $k,
                    'value' => serialize($settings),
                ]);
            }

            return SettingCollection::make($this->model->where('user_id', $user_id)->get());

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
