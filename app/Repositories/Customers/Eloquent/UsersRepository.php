<?php

namespace App\Repositories\Customers\Eloquent;

use App\Http\Requests\Customer\ActiveRequest;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\Customer\UserRequest;
use App\Http\Resources\Users\UserResource;
use App\Models\User;
use App\Repositories\Customers\UsersRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use App\Services\User\ActivateUser;
use App\Services\User\UserPropertyService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class UsersRepository extends BaseRepository implements UsersRepositoryInterface
{
    private ActivateUser $activateUser;

    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function register(UserRequest $request): JsonResponse
    {
        try {
            $input = $request->all();

            $input['password'] = bcrypt($input['password']);
            $user = $this->model->create($input);

            $success['user'] = UserResource::make($user);
//            send email to user for activation account
            UserPropertyService::sendEmailForActiveUser($user->email);

            return $success;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function login(LoginRequest $request): JsonResponse
    {
        try {
//            check for credential of developer or cabin user
            $email = UserPropertyService::getEmail($request->email);
//            find user
            $currentUser = UserPropertyService::getUser($email);
            $token = UserPropertyService::makeToken($currentUser);
            // remove other tokens
            // send duplicate login mail to other users
            UserPropertyService::afterLoggedInEvents($currentUser);

            $responseMsg = $currentUser->isActive() ? 'user.login' : 'user.not_active';

            //check if user does active 2fa
            if ($currentUser->isActive2fa()) {
                UserPropertyService::makeTwoFaPassword($currentUser, rand(100000, 999999), now()->addMinutes(config('globals.two_factor_auth_time_exp')));

                //send code to user
                UserPropertyService::sendOtp($currentUser);

                $success = [
                    'email' => $currentUser->email,
                    'has_two_factor_auth' => true,
                    'isVerifiedCode' => 'no',
                    'expiration_time' => config('globals.two_factor_auth_time_exp'),
                    'token' => $token,
                    'responseMsg' => $responseMsg
                ];


            }
            $success = [
                'user' => new UserResource($currentUser),
                'token' => $token,   // use sanctum laravel for authorization
                'responseMsg' => $responseMsg
            ];
            return $success;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function logout($id): JsonResponse
    {
        try {
            session()->forget('developer');
            $user = $this->model->findOrFail($id);
            $user->tokens()->delete();
            UserPropertyService::afterLoggedOutEvents($user);

            return true;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

}
