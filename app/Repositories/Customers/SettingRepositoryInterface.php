<?php

namespace App\Repositories\Customers;

use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use Illuminate\Http\JsonResponse;

interface SettingRepositoryInterface
{
    /**
     * user's can store their configurations.
     *
     * @param UserSettingRequest $request
     * @param int $user_id
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/user-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     *
     * @OA\Post  (
     *     path="/en/api/v1/user/auth/configuration",
     *     summary="user can store own configs. for example notifications alarm",
     *     tags={"Cabin: setConfiguration"},
     *     security={{"apiAuth":{}}},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="notification",type="json"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successfully",
     *      @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="authentication failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=403,
     *          description="authorization failed!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=429,
     *          description="too many attempts!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=419 ,
     *          description="token mismatch!",
     *      @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *          response=500 ,
     *          description="internal error",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function setConfiguration(UserSettingRequest $request, int $user_id): JsonResponse;
}
