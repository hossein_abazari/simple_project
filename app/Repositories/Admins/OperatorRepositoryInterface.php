<?php

namespace App\Repositories\Admins;

use App\Http\Requests\Admin\OperatorSelfAuthRequest;
use Illuminate\Http\JsonResponse;

interface OperatorRepositoryInterface
{
    public function profile(int $id): JsonResponse;

    public function update(OperatorSelfAuthRequest $request, int $id): JsonResponse;
}
