<?php

namespace App\Repositories\Admins\Eloquent;

use App\Exceptions\api\v1\QueryNotFoundException;
use App\Http\Requests\Admin\ConfigRequest;
use App\Http\Requests\Admin\LoginAsDeveloperRequest;
use App\Http\Resources\Admins\SettingCollection;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\Admins\SettingRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{
    /**
     * SettingRepository constructor.
     *
     * @param Setting $model
     */
    public function __construct(Setting $model)
    {
        parent::__construct($model);
    }

    public function index(): JsonResponse
    {
        try {
            return SettingCollection::make($this->model->get());
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function storeConfiguration(Request $request): JsonResponse
    {
        try {

            foreach ($request->all() as $k => $settings) {
                $this->model->query()->updateOrCreate(['key'=>$settings['key']], [
                    'type' => $settings['type'],
                    'key' => $settings['key'],
                    'value' => $settings['value'], // this field will be convert to json in mutator
                    'role' => $settings['role'], // this field will be convert to json in mutator
                ]);
            }

            return SettingCollection::make($this->model->get());

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function setLoginAsDeveloper(LoginAsDeveloperRequest $request): JsonResponse
    {
        try {
            $userId = $request->user_id;
            $this->setModel(new User());
            $user = $this->model->query()->find($userId);

            throw_if(! $user, new QueryNotFoundException('user not found!'));
            $this->setModel(new Setting());
            $this->model->findOrUpdateOrCreate('develop_mode_status', [
                'key' => 'develop_mode_status',
                'value' => 'true',
            ]);

            $this->model->findOrUpdateOrCreate('develop_mode_username', [
                'key' => 'develop_mode_username',
                'value' => $user->email,
            ]);

            return [];

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
