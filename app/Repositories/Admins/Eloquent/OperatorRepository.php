<?php

namespace App\Repositories\Admins\Eloquent;

use App\Http\Requests\Admin\OperatorSelfAuthRequest;
use App\Http\Resources\Admins\UserResource;
use App\Models\Admin;
use App\Repositories\Admins\OperatorRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class OperatorRepository extends BaseRepository implements OperatorRepositoryInterface
{
    /**
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        parent::__construct($model);
    }

    public function profile(int $id): JsonResponse
    {
        try {
            app()->setLocale('en');
            $model = $this->model->findOrFail($id);

            return UserResource::collection(collect([$model]));

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function update(OperatorSelfAuthRequest $request, int $id): JsonResponse
    {
        try {
            $model = $this->model->findOrFail($id);
            $model->name = $request->name;
            $model->password = bcrypt($request->password);
            $model->save();

            return \App\Http\Resources\Admins\UserResource::collection(collect([$model]));
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
