<?php

namespace App\Repositories\Admins\Eloquent;

use App\Exceptions\api\v1\OperatorNotFoundException;
use App\Exports\UsersExport;
use App\Http\Requests\Admin\AdminAuthRequest;
use App\Http\Requests\Admin\AdminSecurityProfileRequest;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\OperatorAuthRequest;
use App\Http\Requests\Admin\PinCodeMobileRequest;
use App\Http\Requests\Admin\ProfileSearchRequest;
use App\Http\Requests\Admin\TwoFactorAuthRequest;
use App\Http\Requests\Customer\AdminGeneralProfileRequest;
use App\Http\Requests\Customer\ForeignProfileRequest;
use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use App\Http\Resources\Admins\LoginAnswerCollection;
use App\Http\Resources\Admins\OperatorResource;
use App\Http\Resources\Admins\OperatorsCollection;
use App\Http\Resources\Admins\SearchCollection;
use App\Http\Resources\Admins\UserProfileResource as UserProfileResourceAlias;
use App\Http\Resources\Admins\UserResource;
use App\Http\Resources\Users\SettingCollection;
use App\Http\Resources\Users\UserDocumentResource;
use App\Models\Admin;
use App\Models\CountryUser;
use App\Models\FatfAnswer;
use App\Models\FatfQuestion;
use App\Models\LoginQuestion;
use App\Models\PinCode;
use App\Models\TwoFactor;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserSetting;
use App\Repositories\Admins\AuthRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use App\Services\Documents\BanksService;
use App\Services\Documents\GeneralsService;
use App\Services\Documents\LegalsService;
use App\Services\Documents\ResidenciesService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AuthRepository extends BaseRepository implements AuthRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        parent::__construct($model);
    }

    public function registerOperator(OperatorAuthRequest $request): JsonResponse
    {
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = $this->model->create($input);

            //        assign role to operator if register operator
            $user->assignRole([$request->role_id]);

            $success['token'] = $user->createToken('app', ['role:operator'])->plainTextToken;  // use sanctum laravel for authorization
            $success['name'] = $user->name;

            return  $success;
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function getAllOperators(): JsonResponse
    {
        try {
            $operators = $this->model->where('is_super', 0)->with('roles')->get();

            return OperatorsCollection::make($operators);
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function getOperator($id): JsonResponse
    {
        try {
            $operator = $this->model->with('roles')->findOrFail($id);

            return OperatorResource::make($operator);
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function export(Request $request): BinaryFileResponse
    {
        return Excel::download(new UsersExport(), 'users.xlsx');
    }

    public function updateOperator(OperatorAuthRequest $request, $id): JsonResponse
    {
        try {
            $model = $this->model->findOrFail($id);
            $model->name = $request->name;
            $model->email = $request->email;
            $model->password = bcrypt($request->password);
            $model->save();
            $model->syncRoles([$request->role_id]);

            return UserResource::collection(collect([$model]));
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function update(AdminAuthRequest $request, int $id): JsonResponse
    {
        try {
            $model = $this->model->findOrFail($id);
            $model->name = $request->name;
            $model->email = $request->email;
            $model->password = bcrypt($request->password);
            $model->save();

            return UserResource::collection(collect([$model]));

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $admin = $this->model->where('email', $request->email)->first();
            $token = $admin->createToken('admin', ['role:admin'])->plainTextToken;

            // check for active or not active operator
            if ($admin->isOperator() && ! $admin->isActiveOperator()) {
                $success = [
                    'questions' => new LoginAnswerCollection(LoginQuestion::all()),
                    'operator' => UserResource::collection(collect([$admin])),
                    'token' => $token,   // use sanctum laravel for authorization
                ];

            }

            $success = [
                'admin' => $admin,
                'token' => $token,   // use sanctum laravel for authorization
            ];


            return $success;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function profile($id): JsonResponse
    {
        try {
            return [$this->model->findOrFail($id)];
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function destroy($id): JsonResponse
    {
        try {
            $model = $this->model->find($id);
            throw_if(! $model, new OperatorNotFoundException('operator not found'));
            $model->delete();

            return true;

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    public function logout($id): JsonResponse
    {
        try {
            $admin = $this->model->findOrFail($id);
            $admin->tokens()->delete();

            $responseMessage = $admin->isOperator() ? 'auth.operator.signout' : 'auth.admin.signout';

            return $responseMessage;
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }






}
