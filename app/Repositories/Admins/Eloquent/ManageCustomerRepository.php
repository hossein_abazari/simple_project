<?php

namespace App\Repositories\Admins\Eloquent;

use App\Http\Requests\Admin\ManageCustomerRequest;
use App\Http\Resources\Admins\ManageCustomerCollection;
use App\Models\Log;
use App\Models\User;
use App\Repositories\Admins\ManageCustomerRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ManageCustomerRepository extends BaseRepository implements ManageCustomerRepositoryInterface
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Created_at query filter set to carbon sub days, month, week
     *
     * @param $request
     * @return mixed
     */
    public function getRegisteredCustomers(ManageCustomerRequest $request) :JsonResponse
    {
        try {
            $sub = 'sub'.ucwords($request->date_sub);

            $model = $this->model
                ->latest()
                ->where('created_at', '<=', Carbon::now())
                ->where('created_at', '>=', Carbon::now()->{$sub}()->toDate())
                ->get();

            return ManageCustomerCollection::make($model);

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    /**
     * Inactive Customer
     *
     * @return mixed
     */
    public function getInactiveCustomers() :JsonResponse
    {
        try {
            $model = $this->model->latest()->whereIs_active(0)->get();

            return  ManageCustomerCollection::make($model);

        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }

    /**
     * Inactive Customer with id
     *
     * @param $id
     * @return JsonResponse
     */
    public function getInactiveCustomerUser($id) :JsonResponse
    {
        try {
            $model = $this->model->latest()->whereId($id)->whereIs_active(0)->get();

            return ManageCustomerCollection::make($model);
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
