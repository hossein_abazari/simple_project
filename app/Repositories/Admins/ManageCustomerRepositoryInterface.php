<?php

namespace App\Repositories\Admins;

use App\Http\Requests\Admin\ManageCustomerRequest;
use Illuminate\Http\JsonResponse;

interface ManageCustomerRepositoryInterface
{
    public function getRegisteredCustomers(ManageCustomerRequest $request) :JsonResponse;

    public function getInactiveCustomers() :JsonResponse;

    public function getInactiveCustomerUser($id) :JsonResponse;
}
