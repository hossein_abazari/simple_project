<?php

namespace App\Repositories\Admins;

use App\Http\Requests\Admin\LoginAsDeveloperRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface SettingRepositoryInterface
{
    /**
     * Login as Developer
     *
     * @param LoginAsDeveloperRequest $request
     * @return JsonResponse
     *
     * @OA\Post (
     *     path="/api/v1/admin/auth/login-as-developer",
     *     summary="Set login as developer",
     *     tags={"Admin"},
     *     security={{"apiAuth":{}
     *     }},
     *      @OA\RequestBody (
     *          @OA\JsonContent (
     *              type="object",
     *              @OA\Property (property="id", type="integer", default="1"),
     *          )
     *      ),
     *     @OA\Response(
     *         response=201,
     *          description="successful registered",
     *     ),
     * )
     *
     *
     * @copyright 2021
     * @filesource routes/admin-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     */
    public function setLoginAsDeveloper(LoginAsDeveloperRequest $request): JsonResponse;

    public function index(): JsonResponse;

//    public function storeConfiguration(ConfigRequest $request): JsonResponse;
    public function storeConfiguration(Request $request): JsonResponse;
}
