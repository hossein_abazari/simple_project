<?php

namespace App\Repositories\Admins;

use App\Http\Requests\Admin\AdminAuthRequest;
use App\Http\Requests\Admin\AdminSecurityProfileRequest;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\OperatorAuthRequest;
use App\Http\Requests\Admin\PinCodeMobileRequest;
use App\Http\Requests\Admin\ProfileSearchRequest;
use App\Http\Requests\Admin\TwoFactorAuthRequest;
use App\Http\Requests\Customer\AdminGeneralProfileRequest;
use App\Http\Requests\Customer\ForeignProfileRequest;
use App\Http\Requests\Customer\GeneralProfileRequest;
use App\Http\Requests\Customer\SecurityProfileRequest;
use App\Http\Requests\Customer\SettingRequest as UserSettingRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

interface AuthRepositoryInterface
{
    public function registerOperator(OperatorAuthRequest $request): JsonResponse;

    public function getAllOperators(): JsonResponse;

    public function updateOperator(OperatorAuthRequest $request, $id): JsonResponse;

    public function getOperator($id): JsonResponse;

    public function update(AdminAuthRequest $request, int $id): JsonResponse;

    public function login(LoginRequest $request): JsonResponse;

    public function destroy(int $id): JsonResponse;

    public function profile(int $id): JsonResponse;

    /**
     * Logout user with remove its session and token
     *
     * @param int $id
     * @return JsonResponse
     * @copyright 2021
     * @filesource routes/admin-api.php
     * @api
     * @author Majid Kashefy <kashefymajid1992@gmail.com>
     */
    public function logout(int $id): JsonResponse;

    public function export(Request $request): BinaryFileResponse;


}
