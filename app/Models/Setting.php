<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property int|null $module_id
 * @property string|null $key
 * @property string|null $value
 * @property string $type
 * @property string $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereValue($value)
 * @mixin \Eloquent
 */
class Setting extends BasicModel
{
    use HasFactory;

    protected $primaryKey = 'key';

    public $incrementing = false;

    protected $fillable = ['key', 'module_id', 'value', 'type', 'role'];

//    protected $casts = ['value' => 'array'];

    public static function findOrUpdateOrCreate($key, $values)
    {
        $obj = static::whereId($key)->first();

        if (! $obj) {
            return static::create($values);
        }

        return $obj->update($values);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }

    public function getValueAttribute($value)
    {
        return unserialize($value);
//        return unserialize(trim(str_replace(DIRECTORY_SEPARATOR, '', $value), '"'));
    }

//    public function getKeyAttribute(){
//        return trans('settings.general.'.$this->id);
//    }
}
