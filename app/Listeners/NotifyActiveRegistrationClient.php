<?php

namespace App\Listeners;

use App\Events\ActiveRegistrationClient;
use App\Jobs\ActiveUserMailJob;
use App\Services\SendTokenEmail;
use App\Traits\Api\v1\ResponseExceptionError;
use App\Traits\Api\v1\Responsible;

class NotifyActiveRegistrationClient
{
    use Responsible, ResponseExceptionError;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param ActiveRegistrationClient $event
     * @return void
     */
    public function handle(ActiveRegistrationClient $event)
    {
        $resetCode = new SendTokenEmail(['email' => $event->email], 'users', $event->resetTable);
        $resetCode->sendMail($event->email, ActiveUserMailJob::class);
    }
}
