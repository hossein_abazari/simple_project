<?php

namespace App\Listeners;

use App\Events\LoggedIn;
use App\Jobs\DuplicatedLoginMailJob;
use App\Traits\Api\v1\ResponseExceptionError;
use App\Traits\Api\v1\Responsible;

class NotifyDuplicatedLogin
{
    use Responsible, ResponseExceptionError;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoggedIn $event
     * @return void
     */
    public function handle(LoggedIn $event)
    {
        $this->sendActivateMail($event->client);
    }

    private function sendActivateMail($client)
    {
        try {
            $email = $client->email;
            dispatch(new DuplicatedLoginMailJob($email, $client));
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
