<?php

namespace App\Listeners;

use App\Events\LoggedIn;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogoutOtherActiveUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoggedIn  $event
     * @return void
     */
    public function handle(LoggedIn $event)
    {
        $currentUser = $event->client;
        $currentToken = $currentUser->tokens()->latest()->first()->token;
//        remove other tokens
        $currentUser->tokens()->where('token', '!=', $currentToken)->delete();
    }
}
