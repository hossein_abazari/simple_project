<?php

namespace App\Listeners;

use App\Events\LoginAnswerCreated;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyLoginAnswerCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginAnswerCreated  $event
     * @return void
     */
    public function handle(LoginAnswerCreated $event)
    {
        $operator = Admin::where('id', $event->operator_id)->update(['is_active'=>true]);
    }
}
