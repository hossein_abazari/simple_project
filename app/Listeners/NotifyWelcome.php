<?php

namespace App\Listeners;

use App\Events\LoggedIn;
use App\Jobs\WelcomeLoginMailJob;
use App\Traits\Api\v1\ResponseExceptionError;
use App\Traits\Api\v1\Responsible;

class NotifyWelcome
{
    use Responsible, ResponseExceptionError;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoggedIn $event
     * @return void
     */
    public function handle(LoggedIn $event)
    {
        try {
            if ($event->client->isNotificationLoginEnable()) {
                $email = $event->client->email;
                dispatch(new WelcomeLoginMailJob($email, $event->client));
            }
        } catch (\Exception $e) {
            return $this::handleError($e);
        }
    }
}
