<?php

namespace App\Services\Admin;

use App\Models\Admin;

class AdminProperty
{
    public static function operatorQuestions($user_id)
    {
        $admin = Admin::find($user_id);
        $questions = collect();

        foreach ($admin->answers()->with('question')->get() as $value) {
            $questions->add($value->question);
        }

        return $questions;
    }

    public static function isCorrectAnswerLogin($expected_answers, $answers): bool
    {
        $count = 0;
        foreach ($expected_answers as $expected_answer) {
            foreach ($answers as $answer) {
                if (empty(array_diff($expected_answer, $answer))) {
                    $count++;
                    break;
                }
            }
        }

        return $count == 3;
    }
}
