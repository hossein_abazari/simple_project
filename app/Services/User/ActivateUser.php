<?php

namespace App\Services\User;

use App\Traits\Api\v1\Responsible;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ActivateUser
{
    use Responsible;

    public $request;

    public $userTable;

    public $resetTable;

    public function __construct($request, $user_table, $reset_table)
    {
        $this->request = $request;
        $this->userTable = $user_table;
        $this->resetTable = $reset_table;
    }

//    public function generateToken($email, $table)
//    {
//        $isOtherToken = DB::table($table)->where('email', $email)->first();
//        if ($isOtherToken) {
//            return $isOtherToken->token;
//        }
//
//        $token = Str::random(80);
//
//        $this->storeToken($token, $this->request->email, $this->resetTable);
//        return $token;
//    }

//    public function storeToken($token, $email, $table)
//    {
//        DB::table($table)->insert([
//            'email' => $email,
//            'token' => $token,
//            'created_at' => Carbon::now(),
//            'expiration_date' => Carbon::now()->addMinutes(config('globals.reset_pwd_time_exp'))
//        ]);
//    }

    // Verify if token is valid
    public function getPasswordRow()
    {
        return DB::table($this->resetTable)->where(['email' => $this->request->email])->where(['token' => $this->request->token])->where('expiration_date', '>', Carbon::now())->get();
    }

    // delete token
    private function deletePasswordRow()
    {
        return DB::table($this->resetTable)->where(['email' => $this->request->email])->where(['token' => $this->request->token])->where('expiration_date', '>', Carbon::now())->delete();
    }

    // Token not found response
    public function tokenNotFoundError()
    {
        return $this->error(__('auth.token_error'), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    // Reset password
    public function setActivate(): \Illuminate\Http\JsonResponse
    {
        DB::table($this->userTable)
            ->where('email', $this->request->email)
            ->update(['is_active' => true]);
        // update password
        $userData = DB::table($this->userTable)->where('email', $this->request->email)->get();
        // remove verification data from db
        $this->deletePasswordRow();
        // activate user
        return $this->success(__('auth.active_user'), $userData, Response::HTTP_CREATED);
    }
}
