<?php

namespace App\Services\User;

use App\Traits\Api\v1\ResponseExceptionError;
use App\Traits\Api\v1\Responsible;
use Illuminate\Http\Response;

class ResetPinCode
{
    use Responsible, ResponseExceptionError;

    public $request;

    public $baseTable;

    public $resetTable;

    public function __construct(array $request, string $base_table, string $reset_table)
    {
        $this->request = $request;
        $this->baseTable = $base_table;
        $this->resetTable = $reset_table;
    }

    // Token not found response
    public function hasToken()
    {
        return $this->success(__('is ok'), [], Response::HTTP_OK);
    }

    public function changeToken()
    {
    }
}
