<?php

namespace App\Services\User;

use App\Events\ActiveRegistrationClient;
use App\Events\LoggedIn;
use App\Events\LoggedOut;
use App\Facades\NotificationOTP;
use App\Models\Admin;
use App\Models\User;
use App\Services\IdentificationService;

class UserPropertyService
{
    private static Admin $admin;

    private static User $user;

    public function setAdmin(Admin $admin)
    {
        self::$admin = $admin;
    }

    public function setUser(User $user)
    {
        self::$user = $user;
    }

    public static function isLogin()
    {
        // check for login admin or customer
        return auth()->guard('sanctum')->check();
    }

    public static function isDebugging()
    {
        return config('app.debug');
    }

    public static function isSuperAdmin()
    {
        return self::$admin->isSuperAdmin() ?? false;
    }

    public static function isCustomer()
    {
        return auth()->guard('user')->check();
    }

    public static function isDeveloper()
    {
        return session()->get('developer');
    }

    public static function isSetUser()
    {
        return ! empty(self::$user);
    }

    public static function isSetAdmin()
    {
        return ! empty(self::$admin);
    }

    public static function canWatchFullExceptions(): bool
    {
        if (self::isDebugging()) {
            return true;
        }
        if (self::isLogin()) {
            if (self::isSetAdmin()) {
                return (self::isSuperAdmin() && self::isDebugging()) || self::isDeveloper();
            } elseif (self::isSetUser()) {
                return (self::isCustomer() && self::isDebugging()) || self::isDeveloper();
            }
        }

        return false;
    }

    public static function getEmail($email): mixed
    {
        return IdentificationService::isDeveloper() ? IdentificationService::getDeveloperEmail() : $email;
    }

    public static function getUser(string $email)
    {
        return User::query()->where('email', $email)->first();
    }

    public static function getUserByPhone(string $phone)
    {
        return User::query()->where('phone', $phone)->first();
    }

    public static function makeToken(mixed $currentUser): mixed
    {
        return $currentUser->createToken('user', ['role:client'])->plainTextToken;
    }

    public static function makeMobileToken(mixed $currentUser): mixed
    {
        return $currentUser->createToken('user', ['role:mobile'])->plainTextToken;
    }

    public static function afterLoggedInEvents($user): void
    {
        event(new LoggedIn($user));
    }

    public static function afterLoggedOutEvents($user): void
    {
        event(new LoggedOut($user));
    }

    public static function makeTwoFaPassword($user, $random = 1234, $expire = '')
    {
        $user->twoFactorAuth()->update(['code' => $random, 'expire_at' => $expire]);
    }

    public static function makeOtpPassword($user, $random = 1234, $expire = '', $type = 'sms')
    {
        $user->twoFactorAuth()->updateOrCreate(['user_id' => $user->id], ['type' => $type, 'code' => $random, 'expire_at' => $expire]);
    }

    public static function sendOtp($user): void
    {
        session()->put('otp_type', $user->twoFactorAuth->type);
        NotificationOTP::send($user, $user->twoFactorAuth->code);
    }

    public static function sendEmailForActiveUser($user_email): void
    {
        event(new ActiveRegistrationClient($user_email, 'password_resets')); // for activation user
    }

    public static function sendSmsForActivePhone($receiverNumber, $code)
    {
        session()->put('otp_type', 'sms');
        NotificationOTP::send($receiverNumber, $code);
    }

    public static function strSlugToken($number = null)
    {
        if ($number != null && ! is_string($number)) {
            return substr(md5(uniqid(mt_rand(), true)), 0, $number);
        }
        return substr(md5(uniqid(mt_rand(), true)), 0, 8);
    }
}
