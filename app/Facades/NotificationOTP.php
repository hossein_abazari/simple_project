<?php

namespace App\Facades;

use App\Services\TwoFactorAuth\CallTwoFactorAuthService;
use App\Services\TwoFactorAuth\EmailTwoFactorAuthService;
use App\Services\TwoFactorAuth\SmsTwoFactorAuthService;
use Illuminate\Support\Facades\Facade;

class NotificationOTP extends Facade
{
    // for send email or sms when user active two factor authentication
    protected static function getFacadeAccessor(): string
    {
        //put session from App\Repositories\...\Eloquent\AuthRepository
        $twoFactorType = session()->get('otp_type'); // email or sms or call
        if ($twoFactorType === 'email') {
            $class = EmailTwoFactorAuthService::class;
        } elseif ($twoFactorType === 'sms') {
//            $class = SmsTwoFactorAuthService::class;
            $class = CallTwoFactorAuthService::class;
        } elseif ($twoFactorType === 'call') {
            $class = CallTwoFactorAuthService::class;
        }

        session()->forget('otp_type');

        return $class;
    }
}
