<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{
    protected array $baseConfigValues = [];

    /**
     * @var array
     */
    protected array $configs = [];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('settings')) {
            $this->setToGeneralConfigFromDB();
        }
    }

    public function setToGeneralConfigFromDB(): void
    {
        //this section for set config from user table
        $this->setUserConfig();

//        set configs by super admin from database
        $this->setConfigsByAdmin();
    }

    private function setConfigParams()
    {
        $this->configs = [
            'globals.app_name' => [ // set config
                'app_name', // get value from DB table key
                config('app.name'), // get default value
            ],
            'globals.develop_mode.status' => ['develop_mode_status', 'false'],
            'globals.upload_size' => ['upload_size', config('globals.upload_size')],
            'globals.develop_mode.username' => ['develop_mode_username', ''],
            'globals.develop_identifications.user_agents' => ['develop_mode_user_agents', []],
            'globals.develop_identifications.ips' => ['develop_mode_ips', []],
            'globals.develop_mode.developer_identification' => ['developer_credential_switch', ''],
            'app.locale' => ['language_basic', app()->getLocale()],
            'app.timezone' => ['timezone', config('app.timezone')],
            'globals.cache.status' => ['caching', config('globals.cache.status')],
            'globals.cache.cache_time' => ['cache_time', config('globals.cache.cache_time')],
            'globals.login_limiter.max' => ['login_limiter_max', config('globals.login_limiter.max')],
            'globals.login_limiter.decay' => ['login_limiter_decay', config('globals.login_limiter.decay')],
            'globals.logs.count' => ['logs_count', config('globals.logs.count')],
            'globals.reset_pwd_time_exp' => ['reset_pwd_time_exp', config('globals.reset_pwd_time_exp')],
            'globals.expire_trial' => ['expire_trial', config('globals.expire_trial')],
            'globals.file_mime_type' => ['file_mime_type', config('globals.file_mime_type')],
            'globals.two_factor_exp' => ['two_factor_exp', config('globals.two_factor_exp')],
            'globals.default_base_url' => ['default_base_url', config('globals.default_base_url')],
            'cache.default' => ['caching', config('cache.default')],
        ];
    }

    private function saveConfigsToFile(array $configs)
    {
        //        set to config file from db
        foreach ($configs as $config => $values) {
            if ($config === 'app.timezone') {
                date_default_timezone_set(\config()->get($config, $this->baseConfigValues[$values[0]] ?? $values[1]));
            } else {
                // save to config from database
                $this::setToConfig($config, $this->baseConfigValues[$values[0]] ?? $values[1]);
            }
        }
    }

    private static function setToConfig(string $config, $setting, $default = false): void
    {
        if (! $default) {
            \config()->set($config, $setting ?? '');
        }
        \config()->set($config, $setting ?? $default);
    }

    private function setUserConfig()
    {
        $baseLang = auth()->guard('user')->check() ? auth()->guard('user')->user()->language : app()->getLocale();
        config()->set('globals.base_language', $baseLang);
    }

    private function setConfigsByAdmin(): void
    {
        $settings = Setting::query()->select(['key', 'value'])->get()->toArray();
        if (! $settings) {
            return;
        }
        //store columns name and configs name
        $this->setConfigParams();

        foreach ($settings as $setting) {
            $key = $setting['key'];
            $values = ($setting['value']);

            $this->baseConfigValues[$key] = $values;
        }

//        save names to file
        $this->saveConfigsToFile($this->configs);
    }
}
