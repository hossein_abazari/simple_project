<?php

namespace App\Providers;

use App\Repositories\Admins\AuthRepositoryInterface;
use App\Repositories\Admins\Eloquent\AuthRepository;
use App\Repositories\Admins\Eloquent\LoginAnswerRepository;
use App\Repositories\Admins\Eloquent\ManageCustomerRepository;
use App\Repositories\Admins\Eloquent\OperatorRepository;
use App\Repositories\Admins\Eloquent\PermissionRepository;
use App\Repositories\Admins\Eloquent\ResetPasswordRepository;
use App\Repositories\Admins\Eloquent\RoleRepository;
use App\Repositories\Admins\Eloquent\SettingRepository;
use App\Repositories\Admins\LoginAnswerRepositoryInterface;
use App\Repositories\Admins\ManageCustomerRepositoryInterface;
use App\Repositories\Admins\OperatorRepositoryInterface;
use App\Repositories\Admins\PermissionRepositoryInterface;
use App\Repositories\Admins\ResetPasswordRepositoryInterface;
use App\Repositories\Admins\RoleRepositoryInterface;
use App\Repositories\Admins\SettingRepositoryInterface;
use App\Repositories\Customers\AuthRepositoryInterface as ClientAuthRepositoryInterface;
use App\Repositories\Customers\Eloquent\AuthRepository as ClientAuthRepository;
use App\Repositories\Customers\Eloquent\PinCodeRepository as ClientPinCodeRepository;
use App\Repositories\Customers\Eloquent\ResetPasswordRepository as ClientResetPasswordRepository;
use App\Repositories\Customers\Eloquent\SettingRepository as ClientSettingRepository;
use App\Repositories\Customers\Eloquent\UserDocumentRepository as ClientDocumentRepository;
use App\Repositories\Customers\Eloquent\UserProfileRepository as ClientProfileRepository;
use App\Repositories\Customers\Eloquent\UsersRepository as ClientUsersRepository;
use App\Repositories\Customers\PinCodeRepositoryInterface as ClientPinCodeRepositoryInterface;
use App\Repositories\Customers\ResetPasswordRepositoryInterface as ClientResetPasswordRepositoryInterface;
use App\Repositories\Customers\SettingRepositoryInterface as ClientSettingRepositoryInterface;
use App\Repositories\Customers\UserDocumentRepositoryInterface as ClientDocumentRepositoryInterface;
use App\Repositories\Customers\UserProfileRepositoryInterface as ClientProfileRepositoryInterface;
use App\Repositories\Customers\UsersRepositoryInterface as ClientUsersRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Mobiles\Eloquent\SettingRepository as MobileSettingRepository;
use App\Repositories\Mobiles\Eloquent\UserDocumentRepository as MobileDocumentRepository;
use App\Repositories\Mobiles\Eloquent\UserProfileRepository as MobileProfileRepository;
use App\Repositories\Mobiles\Eloquent\UsersRepository as MobileUsersRepository;
use App\Repositories\Mobiles\SettingRepositoryInterface as MobileSettingRepositoryInterface;
use App\Repositories\Mobiles\UserDocumentRepositoryInterface as MobileDocumentRepositoryInterface;
use App\Repositories\Mobiles\UserProfileRepositoryInterface as MobileProfileRepositoryInterface;
use App\Repositories\Mobiles\UsersRepositoryInterface as MobileUsersRepositoryInterface;
use App\Repositories\TestRepository;
use App\Repositories\TestRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    private static array $adminBinds = [
        [AuthRepositoryInterface::class => AuthRepository::class],
        [RoleRepositoryInterface::class => RoleRepository::class],
        [PermissionRepositoryInterface::class => PermissionRepository::class],
        [OperatorRepositoryInterface::class => OperatorRepository::class],
        [LoginAnswerRepositoryInterface::class => LoginAnswerRepository::class],
        [ResetPasswordRepositoryInterface::class => ResetPasswordRepository::class],
        [SettingRepositoryInterface::class => SettingRepository::class],
        [ManageCustomerRepositoryInterface::class => ManageCustomerRepository::class],
    ];

    private static array $clientBinds = [
        [ClientUsersRepositoryInterface::class => ClientUsersRepository::class],
        [ClientPinCodeRepositoryInterface::class => ClientPinCodeRepository::class],
        [ClientAuthRepositoryInterface::class => ClientAuthRepository::class],
        [ClientProfileRepositoryInterface::class => ClientProfileRepository::class],
        [ClientSettingRepositoryInterface::class => ClientSettingRepository::class],
        [ClientResetPasswordRepositoryInterface::class => ClientResetPasswordRepository::class],
        [ClientDocumentRepositoryInterface::class => ClientDocumentRepository::class],

    ];

    private static array $mobileBinds = [
        [MobileUsersRepositoryInterface::class => MobileUsersRepository::class],
        [MobileSettingRepositoryInterface::class => MobileSettingRepository::class],
        [MobileProfileRepositoryInterface::class => MobileProfileRepository::class],
        [MobileDocumentRepositoryInterface::class => MobileDocumentRepository::class],
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        //        admin repository
        $this->adminBind();

//        client repository
        $this->clientBind();

        //mobile repos
        $this->mobileBind();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function adminBind(): void
    {
        foreach (self::$adminBinds as $adminBind) {
            $this->app->bind(array_key_first($adminBind), array_values($adminBind)[0]);
        }
    }

    private function clientBind(): void
    {
        foreach (self::$clientBinds as $clientBind) {
            $this->app->bind(array_key_first($clientBind), array_values($clientBind)[0]);
        }
    }

    private function mobileBind(): void
    {
        foreach (self::$mobileBinds as $mobileBind) {
            $this->app->bind(array_key_first($mobileBind), array_values($mobileBind)[0]);
        }
    }
}
