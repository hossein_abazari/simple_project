<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActiveRegistrationClient
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $resetTable;

    public $email;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $reset_table)
    {
        $this->email = $email;
        $this->resetTable = $reset_table;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
