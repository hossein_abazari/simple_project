<?php

namespace App\Rules;

use App\Services\User\UserPropertyService;
use Illuminate\Contracts\Validation\Rule;

class FindUserBPhone implements Rule
{
    private $value;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;

        return $this->check($this->value);
    }

    public function check()
    {
        $currentUser = UserPropertyService::getUserByPhone($this->value);

        return ! is_null($currentUser);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('user.notfound');
    }
}
