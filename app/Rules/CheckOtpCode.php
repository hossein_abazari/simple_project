<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckOtpCode implements Rule
{
    private $user;

    private $response;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_null($this->user)) {
            return true;
        }

        return $this->checkSameCode($this->user, $value);
    }

    public function checkSameCode($user, $otp_code)
    {
        if (! $user->twoFactorAuth()->exists()) {
            $this->response = 'user.otpcode.notfound,401';

            return false;
        }
        $this->response = 'user.otpcode.wrong';
        $userOtpCode = $user->twoFactorAuth->code;

        return $userOtpCode == $otp_code;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __($this->response);
    }
}
