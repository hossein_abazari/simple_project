<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class VerifyTwoFactorCode implements Rule
{
    protected $email;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $user = User::where('email', $this->email)->first();
        $towFactor = $user->twoFactorAuth->where($attribute, $value)->first();
        if (! $towFactor) {
            return false;
        } elseif ($towFactor->expire_at < now()) {
            session()->forget('developer');
            $user->tokens()->delete();

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('code is not correct');
    }
}
