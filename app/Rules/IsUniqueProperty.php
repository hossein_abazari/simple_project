<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class IsUniqueProperty implements Rule
{
    private $field;

    private $table;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $field, string $table)
    {
        $this->field = $field;
        $this->table = $table;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exist = DB::table($this->table)->where($this->field, $value)->first();

        return ! empty($exist);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('this is exists in database!');
    }
}
