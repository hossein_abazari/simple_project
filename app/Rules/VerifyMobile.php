<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VerifyMobile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $code = \auth()->guard('user')->user()->mobile->code;

        return $code == Request()->code;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Verification code is wrong.');
    }
}
