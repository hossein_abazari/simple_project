<?php

namespace App\Rules;

use App\Models\Admin;
use App\Services\Admin\AdminProperty;
use Illuminate\Contracts\Validation\Rule;

class CheckOperatorAnswers implements Rule
{
    private $answers;

    private $email;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $answers, string $email)
    {
        $this->answers = $answers;
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->check($this->answers, $this->email);
    }

    public function check($answers, $email)
    {
        $operator = Admin::where('email', $email)->first();
        $expectedAnswers = $operator->specificAnswers->toArray();

        return AdminProperty::isCorrectAnswerLogin($expectedAnswers, $answers);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('answers in not correct');
    }
}
