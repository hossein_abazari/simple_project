<?php

namespace App\Rules;

use App\Models\RefreshToken;
use Illuminate\Contracts\Validation\Rule;

class CheckRefreshToken implements Rule
{
    private $value;

    private $response;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
//
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;

        return $this->check();
    }

    public function check()
    {
        $refreshToken = RefreshToken::where('refresh_token', $this->value)->first();

        if (is_null($refreshToken)) {
            $this->response = 'user.refresh_token.notfound';

            return false;
        }

        $this->response = 'user.refresh_token.expired';

        return now() < $refreshToken->refresh_expired_at;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __($this->response);
    }
}
