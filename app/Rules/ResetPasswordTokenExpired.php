<?php

namespace App\Rules;

use App\Services\isExpiredToken;
use Illuminate\Contracts\Validation\Rule;

class ResetPasswordTokenExpired implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return isExpiredToken::check('password_resets', Request()->all());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Token is expired');
    }
}
