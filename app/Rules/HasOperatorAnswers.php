<?php

namespace App\Rules;

use App\Models\Admin;
use App\Services\Admin\AdminProperty;
use Illuminate\Contracts\Validation\Rule;

class HasOperatorAnswers implements Rule
{
    private $answers;

    private $email;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->check($this->email);
    }

    public function check($email)
    {
        $operator = Admin::where('email', $email)->first();
        if (! $operator) {
            return false;
        }
        $expectedAnswers = $operator->specificAnswers->toArray();

        return ! empty($expectedAnswers);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('there is not any answers');
    }
}
