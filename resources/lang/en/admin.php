<?php

return [

    'dont_access' => 'You dont have access!',

    'roles' => [
        'index' => 'All roles',
        'store' => 'New role created!',
        'show' => 'Get role by id',
        'delete' => 'role is deleted!',
        'update' => 'role is updated!',
    ],

    'permissions' => [
        'index' => 'All permissions',
        'store' => 'New permission created!',
        'show' => 'Get permission by id',
        'delete' => 'permission is deleted!',
        'update' => 'permission is updated!',
    ],

    'operator' => [
        'show' => 'operator details!',
        'update' => 'operator is updated!',
        'register' => 'operator register successfully.',
        'questions' => 'please answer this questions to continue.',
        'answered' => 'you dont need answer the questions!!!',
        'questions_login' => 'operator is login but please answer this questions to continue.',
        'answer_login_created' => 'new answer was created and you can access to panel',
        'forget_pass_error' => 'answers is not correct!',
        'forget_pass_error_token' => 'token is wrong!!!',
        'change_pass_success' => 'password has been updated.',
        'change_pass_exptoken' => 'the token has expired!!!',
        'forget_pass_send' => 'the token was sent to your email!',
    ],

];
