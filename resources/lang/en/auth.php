<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'incorrect_credential' => 'The provided credentials are incorrect.',
    'forget_pass_error_token' => 'token is wrong!!!',
    'change_pass_success' => 'password has been updated.',

    'admin' => [
        'signout' => 'Admin successfully signed out.',
        'login' => 'Admin login successfully.',
        'register' => 'Admin register successfully.',
        'update' => 'Admin update successfully.',
        'delete' => 'Admin delete successfully.',
        'details' => 'Admin details is: ',
    ],

    'operator' => [
        'login' => 'Operator login successfully.',
        'signout' => 'Operator successfully signed out.',
    ],
];
