<?php

return [

    'reset_password.title' => ' Reset or change your password.',
    'reset_password.button_title' => 'Change Password',
    'reset_password.greeting' => 'Thanks,',
];
