<?php

return [

    'roles.index' => 'تمامی رول ها',
    'role.store' => 'New role created!',
    'role.show' => 'Get role by id',
    'role.delete' => 'role is deleted!',
    'role.update' => 'role is updated!',

    'permissions.index' => 'All permissions',
    'permission.store' => 'New permission created!',
    'permission.show' => 'Get permission by id',
    'permission.delete' => 'permission is deleted!',
    'permission.update' => 'permission is updated!',

    //    'operator.show' => 'جزییات اوپراتور',
    'operator' => [
        'show'=>'جزییات اوپراتور',
        'update' => 'operator is updated!',
        'register' => 'operator register successfully.',
        'questions' => 'please answer this questions to continue.',
        'answered' => 'you dont need answer the questions!!!',
        'questions_login' => 'اوپراتور لاگین شده است اما باید برای فعال سازی سوالات را جواب دهد.',
        'answer_login_created' => 'new answer was created and you can access to panel',
        'forget_pass_error' => 'answers is not correct!',
        'forget_pass_error_token' => 'token is wrong!!!',
        'change_pass_success' => 'password has been updated.',
        'change_pass_exptoken' => 'the token has expired!!!',
        'forget_pass_send' => 'the token was sent to your email!',
    ],

];
