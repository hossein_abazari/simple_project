<?php

return [

    'dont_access' => 'You dont have access!',

    'show' => 'user details!',
    'update' => 'user is updated!',
    'register' => 'an account activation email has been sent to you ',
    'questions' => 'please answer this questions to continue.',
    'answered' => 'you dont need answer the questions!!!',
    'questions_login' => 'user is login but please answer this questions to continue.',
    'answer_login_created' => 'new answer was created and you can access to panel',
    'forget_pass_error' => 'answers is not correct!',
    'forget_pass_error_token' => 'token is wrong!!!',
    'change_pass_success' => 'password has been updated.',
    'change_pass_exptoken' => 'the token has expired!!!',
    'forget_pass_send' => 'the token was sent to your email!',
    'login' => 'کاربر با موفقیت وارد شد.',
    'not_active' => 'کاربر غیر فعال میباشد.',

    //    rate limitation
    'limit_attempting'=> 'too attempting error',

];
