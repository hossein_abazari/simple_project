<?php

return [
    'documents' => [
        'identities' => [
            '1' => "Driver's license",
            '2' => 'Nationality Identification Card',
            '3' => 'Passport',
            '4' => 'Provisional Card',
        ],
        'companies' => [
            '1' => 'تضامنی',
            '2' => 'تعاونی',
            '3' => 'سایر موارد',
            '4' => 'سهامی خاص',
            '5' => 'سهامی عام',
            '6' => 'مختلط سهامی',
            '7' => 'مختلط غیرسهامی',
            '8' => 'مسئولیت محدود',
            '9' => 'نسبی',
        ],

    ],
    'profiles' =>[
        'trade_styles'=>[
            '1' => 'Scalping',
            '2' => 'Day Trading',
            '3' => 'Swinging',
            '4' => 'A combination of the above',
        ],
        'trade_assets'=>[
            '1' => 'Commodities',
            '2' => 'Currencies',
            '3' => 'Shares',
            '4' => 'Indices',
        ],
        'hears'=>[
            '1' => 'Others',
            '2' => 'YouTube',
            '3' => 'Website/Search Engine',
            '4' => 'Twitter',
            '5' => 'TV/Cable News',
            '6' => 'Newspaper Story',
            '7' => 'Magazine Article',
            '8' => 'Family or Friend',
            '9' => 'Facebook',
            '10' => 'Email/Newsletter',
            '11' => 'Advertisement',
        ],
        'experiences'=>[
            '1' => 'هیچکدام',
            '2' => '1-3 سال(s)',
            '3' => '3-5 سال',
            '4' => '5-10 سال(s)',
            '5' => '10 Or سال Year(s)',
        ],
        'genders'=>[
            '1' => 'Male',
            '2' => 'Female',
        ],
    ],
    'fatf' => [
        'questions' => [
            '1' => 'تست',
            '2' => 'Are you a Resident of the United States of America?',
            '3' => 'Do you hold the United States of America Green Card?',
            '4' => 'Are you a Tax Payer in the United States of America?',
            '5' => 'Were you born in the United States of America?',
        ],
    ],
    'login' => [
        'questions' => [
            '1' => 'test1',
            '2' => 'test2',
            '3' => 'test3',
            '4' => 'test4',
            '5' => 'test5',
        ],
    ],
    'general' => [
        '1' => 'laravel',
        '2' => 'test2',
        '3' => 'test3',
        '4' => 'test4',
        '5' => 'test5',
    ],

    'traderRequest' => [
        'buy' => 'خرید',
        'B' => 'خرید',
        'sell' => 'فروش',
        'S' => 'فروش',
    ]


];
