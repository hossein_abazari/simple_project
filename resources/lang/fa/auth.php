<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'incorrect_credential' => 'The provided credentials are incorrect.',
    'admin.signout' => 'Admin successfully signed out.',
    'admin.login' => 'ادمین با موفقیت وارد شد',
    'admin.register' => 'Admin register successfully.',
    'admin.update' => 'ویرایش ادمین با موفقیت انجام شد.',
    'admin.delete' => 'Admin delete successfully.',
    'admin.details' => 'جزییات ادمین :',

    'admin' => [
        'signout' => 'Admin successfully signed out.',
        'login' => 'ادمین با موفقیت وارد شد.',
        'register' => 'Admin register successfully.',
        'update' => 'Admin update successfully.',
        'delete' => 'Admin delete successfully.',
        'details' => 'Admin details is: ',
    ],

    'operator' => [
        'login' => 'اوپراتور با موفقیت وارد شد.',
        'signout' => 'اوپراتور با موفقیت خارج شد.',
    ],

];
